/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "wallets_history")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WalletsHistory.findAll", query = "SELECT w FROM WalletsHistory w")
    , @NamedQuery(name = "WalletsHistory.findByEntryId", query = "SELECT w FROM WalletsHistory w WHERE w.entryId = :entryId")
    , @NamedQuery(name = "WalletsHistory.findByCreated", query = "SELECT w FROM WalletsHistory w WHERE w.created = :created")
    , @NamedQuery(name = "WalletsHistory.findByWalletId", query = "SELECT w FROM WalletsHistory w WHERE w.walletId = :walletId")
    , @NamedQuery(name = "WalletsHistory.findByWalletIds", query = "SELECT w FROM WalletsHistory w WHERE w.walletId IN :walletIds ORDER BY w.entryId DESC")
        , @NamedQuery(name = "WalletsHistory.countByWalletIds", query = "SELECT COUNT(w) FROM WalletsHistory w WHERE w.walletId IN :walletIds ORDER BY w.entryId DESC")
        , @NamedQuery(name = "WalletsHistory.findByAmount", query = "SELECT w FROM WalletsHistory w WHERE w.amount = :amount")
    , @NamedQuery(name = "WalletsHistory.findByBalanceAfter", query = "SELECT w FROM WalletsHistory w WHERE w.balanceAfter = :balanceAfter")
    , @NamedQuery(name = "WalletsHistory.findByComment", query = "SELECT w FROM WalletsHistory w WHERE w.comment = :comment")
    , @NamedQuery(name = "WalletsHistory.findByUser", query = "SELECT w FROM WalletsHistory w WHERE w.comment = :comment")})
public class WalletsHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "entry_id")
    private Integer entryId;
    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;/*
    @Basic(optional = false)
    @NotNull
    @Column(name = "wallet_id")
    private int walletId;*/
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "balance_after")
    private BigDecimal balanceAfter;
    @Size(max = 255)
    @Column(name = "comment")
    private String comment;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="wallet_id")
    private Wallets walletId;

    public WalletsHistory() {
    }

    public WalletsHistory(Integer entryId) {
        this.entryId = entryId;
    }

    public WalletsHistory(Integer entryId, int walletId) {
        this.entryId = entryId;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Wallets getWallet() {
        return walletId;
    }

    public void setWallet(Wallets walletId) {
        this.walletId = walletId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (entryId != null ? entryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WalletsHistory)) {
            return false;
        }
        WalletsHistory other = (WalletsHistory) object;
        if ((this.entryId == null && other.entryId != null) || (this.entryId != null && !this.entryId.equals(other.entryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.WalletsHistory[ entryId=" + entryId + " ]";
    }
    
}
