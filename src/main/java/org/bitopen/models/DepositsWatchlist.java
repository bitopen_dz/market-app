/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "deposits_watchlist")
@XmlRootElement//
@NamedQueries({
    @NamedQuery(name = "DepositsWatchlist.findAll", query = "SELECT d FROM DepositsWatchlist d")
    , @NamedQuery(name = "DepositsWatchlist.findByTxid", query = "SELECT d FROM DepositsWatchlist d WHERE d.txid = :txid")
    , @NamedQuery(name = "DepositsWatchlist.findByCurrencyId", query = "SELECT d FROM DepositsWatchlist d WHERE d.currencyId = :currencyId")
    , @NamedQuery(name = "DepositsWatchlist.findByCreated", query = "SELECT d FROM DepositsWatchlist d WHERE d.created = :created")
    , @NamedQuery(name = "DepositsWatchlist.findByConfirmations", query = "SELECT d FROM DepositsWatchlist d WHERE d.confirmations = :confirmations")
    , @NamedQuery(name = "DepositsWatchlist.findByStatus", query = "SELECT d FROM DepositsWatchlist d WHERE d.status = :status")
    , @NamedQuery(name = "DepositsWatchlist.findByUserIdCurrencyAndStatus", query = "SELECT d FROM DepositsWatchlist d WHERE d.status = :status AND d.currencyId = :currencyId AND d.userId = :userId ORDER by d.created DESC")
    , @NamedQuery(name = "DepositsWatchlist.findByUserIdCurrency", query = "SELECT d FROM DepositsWatchlist d WHERE d.currencyId = :currencyId AND d.userId = :userId ORDER by d.created DESC")})
public class DepositsWatchlist implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "txid")
    private String txid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "currency_id")
    private String currencyId;
    
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    @Column(name = "last_check")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastCheck;
    @Column(name = "confirmations")
    private Integer confirmations;
    @Basic(optional = false)
    @NotNull
    //@Size(min = 1, max = 8)
    @Column(name = "status")
    private String status;

    public DepositsWatchlist() {
    }

    public DepositsWatchlist(String txid) {
        this.txid = txid;
    }
    
    public DepositsWatchlist(String txid, String currencyId) {
        this.txid = txid;
        this.currencyId = currencyId;
        this.status = "PENDING";
        this.confirmations = 0;
    }

    public DepositsWatchlist(String txid, String currencyId, Date created, String status) {
        this.txid = txid;
        this.currencyId = currencyId;
        this.created = created;
        this.status = status;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Date getCreated() {
        return created;
    }

    public Date getLastCheck() {
        return lastCheck;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(Integer confirmations) {
        this.confirmations = confirmations;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLastCheck(Date lastCheck) {
        this.lastCheck = lastCheck;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (txid != null ? txid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DepositsWatchlist)) {
            return false;
        }
        DepositsWatchlist other = (DepositsWatchlist) object;
        if ((this.txid == null && other.txid != null) || (this.txid != null && !this.txid.equals(other.txid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.DepositsWatchlist[ txid=" + txid + " ]";
    }
    
}
