/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author dzach
 */
@Entity
@Table(name = "withdrawals")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Withdrawals.findAll", query = "SELECT w FROM Withdrawals w")
        , @NamedQuery(name = "Withdrawals.findByWithdrawalId", query = "SELECT w FROM Withdrawals w WHERE w.withdrawalId = :withdrawalId")
        , @NamedQuery(name = "Withdrawals.findByWithdrawalIds", query = "SELECT w FROM Withdrawals w WHERE w.withdrawalId IN :withdrawalIds")
        , @NamedQuery(name = "Withdrawals.findByCurrencyId", query = "SELECT w FROM Withdrawals w WHERE w.currencyId = :currencyId")
        , @NamedQuery(name = "Withdrawals.findByCurrencyIdAndStatus", query = "SELECT w FROM Withdrawals w WHERE w.currencyId = :currencyId AND w.status = :status")
        , @NamedQuery(name = "Withdrawals.findByUserId", query = "SELECT w FROM Withdrawals w WHERE w.userId = :userId")
        , @NamedQuery(name = "Withdrawals.findByUserIdAndCurrencyId", query = "SELECT w FROM Withdrawals w WHERE w.userId = :userId AND w.currencyId = :currencyId")
        , @NamedQuery(name = "Withdrawals.findByAmount", query = "SELECT w FROM Withdrawals w WHERE w.amount = :amount")})
public class Withdrawals implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "withdrawal_id")
    private Integer withdrawalId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "currency_id")
    private String currencyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "title")
    private String title;
    @Column(name = "account_number")
    private String accountNumber;
    @Column(name = "status")
    private String status;
    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public Withdrawals() {
    }

    public Withdrawals(Integer withdrawalId) {
        this.withdrawalId = withdrawalId;
    }

    public Withdrawals(Integer withdrawalId, String currencyId, Long userId) {
        this.withdrawalId = withdrawalId;
        this.currencyId = currencyId;
        this.userId = userId;
    }

    public Integer getWithdrawalId() {
        return withdrawalId;
    }

    public void setWithdrawalId(Integer withdrawalId) {
        this.withdrawalId = withdrawalId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (withdrawalId != null ? withdrawalId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Withdrawals)) {
            return false;
        }
        Withdrawals other = (Withdrawals) object;
        if ((this.withdrawalId == null && other.withdrawalId != null) || (this.withdrawalId != null && !this.withdrawalId.equals(other.withdrawalId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.Withdrawals[ withdrawalId=" + withdrawalId + " ]";
    }

}
