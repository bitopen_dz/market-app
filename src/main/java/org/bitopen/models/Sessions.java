/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "sessions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sessions.findAll", query = "SELECT s FROM Sessions s")
    , @NamedQuery(name = "Sessions.findBySessionId", query = "SELECT s FROM Sessions s WHERE s.sessionId = :sessionId")
    , @NamedQuery(name = "Sessions.findByCreated", query = "SELECT s FROM Sessions s WHERE s.created = :created")
    , @NamedQuery(name = "Sessions.findBySessionToken", query = "SELECT s FROM Sessions s WHERE s.sessionToken = :sessionToken")
    , @NamedQuery(name = "Sessions.findByLastAction", query = "SELECT s FROM Sessions s WHERE s.lastAction = :lastAction")
    , @NamedQuery(name = "Sessions.findByUserId", query = "SELECT s FROM Sessions s WHERE s.userId = :userId")})
public class Sessions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "session_id")
    private Integer sessionId;
    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Size(max = 100)
    @Column(name = "session_token")
    private String sessionToken;
    @Column(name = "last_action")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastAction;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userId;

    public Sessions() {
    }


    public Sessions(String sessionToken, Long userId) {
        this.sessionToken = sessionToken;
        this.userId = userId;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public Date getLastAction() {
        return lastAction;
    }

    public void setLastAction(Date lastAction) {
        this.lastAction = lastAction;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sessionId != null ? sessionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sessions)) {
            return false;
        }
        Sessions other = (Sessions) object;
        if ((this.sessionId == null && other.sessionId != null) || (this.sessionId != null && !this.sessionId.equals(other.sessionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.Sessions[ sessionId=" + sessionId + " ]";
    }
    
}
