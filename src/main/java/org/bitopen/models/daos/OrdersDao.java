package org.bitopen.models.daos;

import org.bitopen.models.Orders;
import org.bitopen.utils.Auth;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zachmian on 30.12.16.
 */
public class OrdersDao {

    public final static BigDecimal MINIMAL_AMOUNT = new BigDecimal(0);

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    public static void setEntityManager(EntityManager entityManager) {
        OrdersDao.entityManager = entityManager;
    }

    public static List<Orders> conditionalGet(String condition, String order) {
        retreiveEntityManager();
        Query query = entityManager.createNativeQuery("SELECT o.order_id, o.rate, o.price, o.amount " +
                "FROM orders o " +
                "WHERE " + condition + " ORDER BY " + order, Orders.class);
        System.out.println("SELECT o.order_id, o.rate, o.price, o.amount " +
                "FROM orders o " +
                "WHERE " + condition + " ORDER BY " + order);
        List<Orders> orders = (List<Orders>) query.getResultList();
        return orders;
    }

    private static void retreiveEntityManager() {
        if (entityManagerFactory == null) {
            entityManagerFactory =
                    Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        }
        if (entityManager == null || !entityManager.isOpen()) {
            entityManager = entityManagerFactory.createEntityManager();
        }
    }

    public static void save(Orders currentOrder) {
        if(currentOrder.getAmount().compareTo(MINIMAL_AMOUNT) <= 0){
            currentOrder.setStatus("EXPIRED");
        }
        entityManager.merge(currentOrder);
    }
}
