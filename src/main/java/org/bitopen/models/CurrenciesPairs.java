/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "currencies_pairs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurrenciesPairs.findAll", query = "SELECT c FROM CurrenciesPairs c")
    , @NamedQuery(name = "CurrenciesPairs.findByPairId", query = "SELECT c FROM CurrenciesPairs c WHERE c.pairId = :pairId")})
public class CurrenciesPairs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "pair_id")
    private String pairId;
    
    @JoinColumn(name = "second_currency_id", referencedColumnName = "currency_id")
    @ManyToOne(optional = false)
    private Currencies secondCurrencyId;
    @JoinColumn(name = "first_currency_id", referencedColumnName = "currency_id")
    @ManyToOne(optional = false)
    private Currencies firstCurrencyId;

    public CurrenciesPairs() {
    }

    public CurrenciesPairs(String pairId) {
        this.pairId = pairId;
    }

    public String getPairId() {
        return pairId;
    }

    public void setPairId(String pairId) {
        this.pairId = pairId;
    }

    public Currencies getSecondCurrencyId() {
        return secondCurrencyId;
    }

    public void setSecondCurrencyId(Currencies secondCurrencyId) {
        this.secondCurrencyId = secondCurrencyId;
    }

    public Currencies getFirstCurrencyId() {
        return firstCurrencyId;
    }

    public void setFirstCurrencyId(Currencies firstCurrencyId) {
        this.firstCurrencyId = firstCurrencyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pairId != null ? pairId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurrenciesPairs)) {
            return false;
        }
        CurrenciesPairs other = (CurrenciesPairs) object;
        if ((this.pairId == null && other.pairId != null) || (this.pairId != null && !this.pairId.equals(other.pairId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.CurrenciesPairs[ pairId=" + pairId + " ]";
    }
    
}
