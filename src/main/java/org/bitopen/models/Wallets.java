/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "wallets")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wallets.findAll", query = "SELECT w FROM Wallets w")
    , @NamedQuery(name = "Wallets.findByWalletId", query = "SELECT w FROM Wallets w WHERE w.walletId = :walletId")
    , @NamedQuery(name = "Wallets.findByCurrencyId", query = "SELECT w FROM Wallets w WHERE w.currencyId = :currencyId")
    , @NamedQuery(name = "Wallets.findByUserId", query = "SELECT w FROM Wallets w WHERE w.userId = :userId")
    , @NamedQuery(name = "Wallets.findByUserAndCurrency", query = "SELECT w FROM Wallets w WHERE w.userId = :userId AND  w.currencyId = :currencyId")
    , @NamedQuery(name = "Wallets.findByBalance", query = "SELECT w FROM Wallets w WHERE w.balance = :balance")
    , @NamedQuery(name = "Wallets.findByLocked", query = "SELECT w FROM Wallets w WHERE w.locked = :locked")
    , @NamedQuery(name = "Wallets.findByAddress", query = "SELECT w FROM Wallets w WHERE w.address = :address")})
public class Wallets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "wallet_id")
    private Integer walletId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "currency_id")
    private String currencyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "balance")
    private BigDecimal balance;
    @Column(name = "locked")
    private BigDecimal locked;
    @Column(name = "address")
    private String address;
    @OneToMany(mappedBy="walletId")
    @JsonIgnore
    Collection<WalletsHistory> historyCollection;


    public Wallets() {
    }

    public Wallets(Integer walletId) {
        this.walletId = walletId;
        this.balance = new BigDecimal(0);
        this.locked = new BigDecimal(0);
    }

    public Wallets(Integer walletId, String currencyId, Long userId) {
        this.walletId = walletId;
        this.currencyId = currencyId;
        this.userId = userId;
        this.balance = new BigDecimal(0);
        this.locked = new BigDecimal(0);
    }
    public Wallets(String currencyId, Long userId) {
        this.currencyId = currencyId;
        this.userId = userId;
        this.balance = new BigDecimal(0);
        this.locked = new BigDecimal(0);
    }
    
    public Wallets(String address, String currencyId, Long userId) {
        this.address = address;
        this.currencyId = currencyId;
        this.userId = userId;
        this.balance = new BigDecimal(0);
        this.locked = new BigDecimal(0);
    }

    public Integer getWalletId() {
        return walletId;
    }

    public void setWalletId(Integer walletId) {
        this.walletId = walletId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Long getUserId() {
        return userId;
    }
    public String getAddress() {
        return address;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getLocked() {
        return locked;
    }

    public void setLocked(BigDecimal locked) {
        this.locked = locked;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Collection<WalletsHistory> getHistoryCollection() {
        return historyCollection;
    }

    public void setHistoryCollection(Collection<WalletsHistory> historyCollection) {
        this.historyCollection = historyCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (walletId != null ? walletId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wallets)) {
            return false;
        }
        Wallets other = (Wallets) object;
        if ((this.walletId == null && other.walletId != null) || (this.walletId != null && !this.walletId.equals(other.walletId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.Wallets[ walletId=" + walletId + " ]";
    }
    
}
