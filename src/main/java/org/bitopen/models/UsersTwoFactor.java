/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "users_two_factor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersTwoFactor.findAll", query = "SELECT u FROM UsersTwoFactor u")
    , @NamedQuery(name = "UsersTwoFactor.findByTwoFactorId", query = "SELECT u FROM UsersTwoFactor u WHERE u.twoFactorId = :twoFactorId")
    , @NamedQuery(name = "UsersTwoFactor.findByUserId", query = "SELECT u FROM UsersTwoFactor u WHERE u.userId = :userId")
    , @NamedQuery(name = "UsersTwoFactor.findBySecretKey", query = "SELECT u FROM UsersTwoFactor u WHERE u.secretKey = :secretKey")})
public class UsersTwoFactor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "two_factor_id")
    private Integer twoFactorId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userId;
    @Size(max = 45)
    @Column(name = "secret_key")
    private String secretKey;

    public UsersTwoFactor() {
    }

    public UsersTwoFactor(Integer twoFactorId) {
        this.twoFactorId = twoFactorId;
    }

    public UsersTwoFactor(Integer twoFactorId, Long userId) {
        this.twoFactorId = twoFactorId;
        this.userId = userId;
    }

    public Integer getTwoFactorId() {
        return twoFactorId;
    }

    public void setTwoFactorId(Integer twoFactorId) {
        this.twoFactorId = twoFactorId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (twoFactorId != null ? twoFactorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersTwoFactor)) {
            return false;
        }
        UsersTwoFactor other = (UsersTwoFactor) object;
        if ((this.twoFactorId == null && other.twoFactorId != null) || (this.twoFactorId != null && !this.twoFactorId.equals(other.twoFactorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.UsersTwoFactor[ twoFactorId=" + twoFactorId + " ]";
    }
    
}
