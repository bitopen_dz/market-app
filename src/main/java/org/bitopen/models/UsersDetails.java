/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "users_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsersDetails.findAll", query = "SELECT u FROM UsersDetails u")
    , @NamedQuery(name = "UsersDetails.findByUserId", query = "SELECT u FROM UsersDetails u WHERE u.userId = :userId")
    , @NamedQuery(name = "UsersDetails.findByFirstname", query = "SELECT u FROM UsersDetails u WHERE u.firstname = :firstname")
    , @NamedQuery(name = "UsersDetails.findByLastname", query = "SELECT u FROM UsersDetails u WHERE u.lastname = :lastname")
    , @NamedQuery(name = "UsersDetails.findByStreet1", query = "SELECT u FROM UsersDetails u WHERE u.street1 = :street1")
    , @NamedQuery(name = "UsersDetails.findByStreet2", query = "SELECT u FROM UsersDetails u WHERE u.street2 = :street2")
    , @NamedQuery(name = "UsersDetails.findByCity", query = "SELECT u FROM UsersDetails u WHERE u.city = :city")
    , @NamedQuery(name = "UsersDetails.findByCountry", query = "SELECT u FROM UsersDetails u WHERE u.country = :country")
    , @NamedQuery(name = "UsersDetails.findByPostcode", query = "SELECT u FROM UsersDetails u WHERE u.postcode = :postcode")
    , @NamedQuery(name = "UsersDetails.findByIdcardNumber", query = "SELECT u FROM UsersDetails u WHERE u.idcardNumber = :idcardNumber")})
public class UsersDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userId;
    @Size(max = 45)
    @Column(name = "firstname")
    private String firstname;
    @Size(max = 45)
    @Column(name = "lastname")
    private String lastname;
    @Size(max = 100)
    @Column(name = "street_1")
    private String street1;
    @Size(max = 100)
    @Column(name = "street_2")
    private String street2;
    @Size(max = 45)
    @Column(name = "city")
    private String city;
    @Size(max = 3)
    @Column(name = "country")
    private String country;
    @Size(max = 25)
    @Column(name = "postcode")
    private String postcode;
    @Size(max = 30)
    @Column(name = "idcard_number")
    private String idcardNumber;

    public UsersDetails() {
    }

    public UsersDetails(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getIdcardNumber() {
        return idcardNumber;
    }

    public void setIdcardNumber(String idcardNumber) {
        this.idcardNumber = idcardNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsersDetails)) {
            return false;
        }
        UsersDetails other = (UsersDetails) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.UsersDetails[ userId=" + userId + " ]";
    }
    
}
