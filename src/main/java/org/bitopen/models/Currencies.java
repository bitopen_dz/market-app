/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.models;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dzach
 */
@Entity
@Table(name = "currencies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Currencies.findAll", query = "SELECT c FROM Currencies c")
    , @NamedQuery(name = "Currencies.findByCurrencyId", query = "SELECT c FROM Currencies c WHERE c.currencyId = :currencyId")
    , @NamedQuery(name = "Currencies.findByIsCrypto", query = "SELECT c FROM Currencies c WHERE c.isCrypto = :isCrypto")})
public class Currencies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "currency_id")
    private String currencyId;
    @Column(name = "is_crypto")
    private Boolean isCrypto;
    @Column(name = "accuracy")
    private Integer accuracy;
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "secondCurrencyId")
    private Collection<CurrenciesPairs> currenciesPairsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "firstCurrencyId")
    private Collection<CurrenciesPairs> currenciesPairsCollection1;
*/
    public Currencies() {
    }

    public Currencies(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Boolean isCrypto() {
        return isCrypto;
    }

    public void setIsCrypto(Boolean isCrypto) {
        this.isCrypto = isCrypto;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Integer getAccuracy() {
        return accuracy;
    }
/*
    @XmlTransient
    public Collection<CurrenciesPairs> getCurrenciesPairsCollection() {
        return currenciesPairsCollection;
    }

    public void setCurrenciesPairsCollection(Collection<CurrenciesPairs> currenciesPairsCollection) {
        this.currenciesPairsCollection = currenciesPairsCollection;
    }

    @XmlTransient
    public Collection<CurrenciesPairs> getCurrenciesPairsCollection1() {
        return currenciesPairsCollection1;
    }

    public void setCurrenciesPairsCollection1(Collection<CurrenciesPairs> currenciesPairsCollection1) {
        this.currenciesPairsCollection1 = currenciesPairsCollection1;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (currencyId != null ? currencyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Currencies)) {
            return false;
        }
        Currencies other = (Currencies) object;
        if ((this.currencyId == null && other.currencyId != null) || (this.currencyId != null && !this.currencyId.equals(other.currencyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.bitopen.models.Currencies[ currencyId=" + currencyId + " ]";
    }
    
}
