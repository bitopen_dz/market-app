package org.bitopen;

import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import org.apache.catalina.WebResource;
import org.bitopen.models.Orders;
import org.bitopen.models.daos.OrdersDao;
import org.bitopen.types.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 *
 * @author dzach
 */

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@ComponentScan(basePackages = "org.bitopen")
@EntityScan("org.bitopen") 
@EnableJpaRepositories("org.bitopen")
public class Application extends WebMvcConfigurerAdapter {
 
    private static final Logger log = LoggerFactory.getLogger(Application.class);
    
    
    public static void main(String[] args) {    
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

    }
    /*
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/").allowedOrigins("http://market.bitopen.pl/");
            }
        };
    }
   
     @Bean
    @Autowired
    public FilterRegistrationBean registerXSSFilter() {
        
        
        FilterRegistrationBean xssFilter = new FilterRegistrationBean();
        xssFilter.setFilter(new XSSFilter());
        xssFilter.setOrder(2); // ordering in the filter chain

        
        return xssFilter;
    }   
    
    */
}
