/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.exceptions;

/**
 *
 * @author dzach
 */
public class RPCInvalidResponseException extends Exception {

    /**
     * Creates a new instance of <code>RPCInvalidResponseException</code>
     * without detail message.
     */
    public RPCInvalidResponseException() {
    }

    /**
     * Constructs an instance of <code>RPCInvalidResponseException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public RPCInvalidResponseException(String msg) {
        super(msg);
    }
}
