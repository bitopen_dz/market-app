/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.exceptions;

/**
 *
 * @author dzach
 */
public class UnknownServiceException extends Exception {

    /**
     * Creates a new instance of <code>UnknownServiceException</code> without
     * detail message.
     */
    public UnknownServiceException() {
    }

    /**
     * Constructs an instance of <code>UnknownServiceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UnknownServiceException(String msg) {
        super(msg);
    }
}
