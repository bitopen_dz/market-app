package org.bitopen.example;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dawid
 */


@RestController
public class ExampleApi {

    private static final Logger LOG = LoggerFactory.getLogger(ExampleApi.class);
    
    
    @RequestMapping( value = "/hello" , method = RequestMethod.GET )
    public @ResponseBody Response index(@RequestParam( value = "name" , required=false)  String name){
        
         
//        LOG.trace("trace log");
//        LOG.debug("debug log"); 
//        LOG.info("info log");               
//        LOG.error("error log");
       
        ///String inputMessage = "input data";
        ///LOG.error("Unable to parse data {}", inputMessage, new Exception("Some Error")); 
        
        return (name == null) ? new Response() : new Response(name);  
    }
    
    @ResponseStatus(HttpStatus.ACCEPTED)
    class Response{
        public String status = "Success";
        public String message;
        
        public Response(){
            message = "Hello :)";
        }
        
        public Response(String name){
            message = "Hello, " + name + " ;)";
        }
    }
    
}
