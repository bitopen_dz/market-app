/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

import java.math.BigDecimal;

/**
 *
 * @author Damian
 */
public class TransactionOutput extends TransactionInput {
    
    public TransactionOutput(String address, BigDecimal amount) {
        super(address, amount);
    }
    
}
