/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

import java.util.HashMap;

/**
 *
 * @author dzach
 */
public class SuccessResponse extends Response {

    public SuccessResponse() {
        super(1);
    }

    public SuccessResponse(String message) {
        super(1, message);
    }

    public SuccessResponse(String message, Object result) {
        super(1, message, result);
    }

    public SuccessResponse(HashMap<String, Object> result) {
        super(1, result);
    }
    
}
