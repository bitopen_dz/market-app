/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

import java.math.BigDecimal;

/**
 *
 * @author Damian
 */
public class TransactionInput {
    String address;
    BigDecimal amount;
    
    public TransactionInput(String address, BigDecimal amount){
        this.address = address;
        this.amount = amount;
    }

    public String getAddress() {
        return address;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    
}
