/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

import java.util.HashMap;

/**
 *
 * @author dzach
 */
public class Response {
    
    private Integer status;
    private String message;
    private Object result;

    public Response() {}
    
    public Response(Integer status) {
        this.status = status;
    }

    public Response(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    public Response(Integer status, String message, Object result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }

    public Response(Integer status, Object result) {
        this.status = status;
        this.result = result;
    }

    
    
    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getResult() {
        return result;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResult(HashMap<String, Object> result) {
        this.result = result;
    }
    
    
}
