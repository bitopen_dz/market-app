/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

import java.util.HashMap;

/**
 *
 * @author dzach
 */
public class ErrorResponse extends Response {

    public ErrorResponse() {
        super(0);
    }

    public ErrorResponse(String message) {
        super(0, message);
    }

    public ErrorResponse(String message, Object result) {
        super(0, message, result);
    }

    public ErrorResponse(Object result) {
        super(0, result);
    }
    
}
