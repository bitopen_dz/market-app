package org.bitopen.types;

import java.security.MessageDigest;
import java.util.HashMap;

/**
 * Created by zachmian on 26.12.16.
 */
public class DotpayStatus {
    public final static Integer ID = 775534;
    public final static String PIN = "tv8jb2GWF1ZteFCpW67bGiPkzKBNzK1u";
    HashMap<String, String> params;

    public DotpayStatus(HashMap<String, String> params){
        this.params = params;
    }

    public Boolean isValid(){
        return params.get("signature").equals(generateSignature())
                && params.get("operation_type").equals("payment")
                && params.get("operation_status").equals("completed");
    }

    public String get(String key){
        return params.get(key);
    }



    private String generateSignature() {
        String[] fields={"id","operation_number","operation_type","operation_status","operation_amount",
                "operation_currency","operation_withdrawal_amount","operation_commission_amount",
                "operation_original_amount","operation_original_currency","operation_datetime",
                "operation_related_number","control","description",
                "email","p_info","p_email","channel","channel_country","geoip_country"};
        StringBuilder sb = new StringBuilder(PIN);
        for (String field : fields) {
            sb.append(params.getOrDefault(field, ""));
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(sb.toString().getBytes());
            byte byteData[] = md.digest();
            sb = new StringBuilder();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }

            sb = new StringBuilder();
            for (byte aByteData : byteData) {
                String hex = Integer.toHexString(0xff & aByteData);
                if (hex.length() == 1) sb.append('0');
                sb.append(hex);
            }

            return sb.toString();
        } catch(Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }


}
