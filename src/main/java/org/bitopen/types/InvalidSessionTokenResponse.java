/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

/**
 *
 * @author dzach
 */
public class InvalidSessionTokenResponse extends ErrorResponse {
    public InvalidSessionTokenResponse(){
        super("Invalid session token");
    }
}
