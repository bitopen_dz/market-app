/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

import java.util.List;

/**
 *
 * @author Damian
 */
public class Transaction {
    Integer confirmations;
    String txid;
    Integer time;
    TransactionInput[] inputs;
    TransactionOutput[] outputs;
    
    public Transaction(){}
    public Transaction(String txid, Integer confirmations){
        this.txid = txid;
        this.confirmations = confirmations;
    }
    public Transaction(String txid, Integer confirmations, Integer time){
        this.txid = txid;
        this.confirmations = confirmations;
        this.time = time;
    }
    public Transaction(String txid, Integer confirmations, Integer time, 
            TransactionInput[] inputs, TransactionOutput[] outputs){
        
        this.txid = txid;
        this.confirmations = confirmations;
        this.inputs = inputs;
        this.outputs = outputs;
        this.time = time;
        
    }

    public Integer getConfirmations() {
        return confirmations;
    }


    public String getTxid() {
        return txid;
    }
    
    public Integer getTime() {
        return time;
    }


    public TransactionInput[] getInputs() {
        return inputs;
    }

    public TransactionOutput[] getOutputs() {
        return outputs;
    }

    public void setConfirmations(Integer confirmations) {
        this.confirmations = confirmations;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }
    
    public void setTime(Integer time) {
        this.time = time;
    }

    public void setInputs(TransactionInput[] inputs) {
        this.inputs = inputs;
    }

    public void setOutputs(TransactionOutput[] outputs) {
        this.outputs = outputs;
    }
    
    
    
    
}
