package org.bitopen.types;

/**
 * Created by zachmian on 26.12.16.
 */
public class SocketMessage {
    private String type;
    private Object content;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
