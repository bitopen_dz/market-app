/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.types;

/**
 *
 * @author Damian
 */
public class BitGoWebhook {
    
    private String type;
    private String walletId;
    private String pendingApprovalId;
    private String hash;

    public String getType() {
        return type;
    }

    public String getWalletId() {
        return walletId;
    }

    public String getHash() {
        return hash;
    }
    
    public String getPendingApprovalId() {
        return pendingApprovalId;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setPendingApprovalId(String pendingApprovalId) {
        this.pendingApprovalId = pendingApprovalId;
    }
    
    
    
}
