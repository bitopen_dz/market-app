/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.bitopen.models.Currencies;
import org.bitopen.models.CurrenciesPairs;
import org.bitopen.models.Users;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dzach
 */
@RestController
@CrossOrigin(origins = "http://market.bitopen.pl")
public class CurrenciesController {
    
    private static EntityManagerFactory factory = null;
    
    @PostConstruct
    void init(){
      factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
    }
    
    @RequestMapping( value = "/currencies" , method = RequestMethod.GET )
    public Response getCurrencies(){
        EntityManager em = factory.createEntityManager();
        try{
            List<Currencies> list = em.createNamedQuery("Currencies.findAll")
                            .getResultList();
            HashMap<String, Object> hm = new HashMap<String, Object>();
            Integer i = 0;
            list.forEach((item) -> {
                hm.put(item.getCurrencyId(), item);
            });
            return new SuccessResponse("Lista walut została pobrana", hm);
        }
        catch(Exception e){
            return new ErrorResponse("Nie udało się pobrać listy walut");
        }
        
    }
    
    @RequestMapping( value = "/currencies/pairs" , method = RequestMethod.GET )
    public Response getPairs(){
        EntityManager em = factory.createEntityManager();
        try{
            List<CurrenciesPairs> list = em.createNamedQuery("CurrenciesPairs.findAll")
                            .getResultList();
            HashMap<String, Object> hm = new HashMap<String, Object>();
            Integer i = 0;
            list.forEach((item) -> {
                hm.put(item.getPairId(), item);
            });
            return new SuccessResponse("Lista par walutowych została pobrana", hm);
        }
        catch(Exception e){
            return new ErrorResponse("Nie udało się pobrać listy par walutowych");
        }
    }
    
}
