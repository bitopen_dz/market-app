/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import org.bitopen.models.Wallets;
import org.bitopen.models.Withdrawals;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.bitopen.utils.Wallet;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author dzach
 */
@RestController
public class WithdrawalsController {
    private static EntityManagerFactory entityManagerFactory = null;
    private static EntityManager entityManager = null;

    @PostConstruct
    public void init() {
        _initEntityManager();
    }

    private static void _initEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
    }


    @RequestMapping(value = "/withdrawals/{currencyId}", method = RequestMethod.GET)
    public Response get(@RequestHeader(value = "Authorization") String token,
                         @PathVariable String currencyId) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {

            entityManager.getTransaction().begin();
            try {
                List withdrawals = entityManager.createNamedQuery("Withdrawals.findByUserIdAndCurrencyId")
                                    .setParameter("currencyId", currencyId)
                                    .setParameter("userId", Auth.getUser().getUserId())
                                    .getResultList();

                entityManager.getTransaction().commit();
                return new SuccessResponse("Wypłaty zostały pobrane", withdrawals);
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie udało się pobrać wypłat");
            }
        }
    }

    @RequestMapping(value = "/withdrawals", method = RequestMethod.POST)
    public Response post(@RequestHeader(value = "Authorization") String token,
                         @RequestParam(value = "key", required = false) String twoFactorKey,
                         @RequestParam(value = "currencyId") String currencyId,
                         @RequestParam(value = "title", required = false) String title,
                         @RequestParam(value = "accountNumber") String accountNumber,
                         @RequestParam(value = "amount") BigDecimal amount) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            if(!isAccountNumberValid(currencyId, accountNumber)) {
                return new ErrorResponse("Podany numer konta jest niepoprawny");
            }
            if (Auth.getTwoFactor().isRequiredAndInvalid("update_details", twoFactorKey)) {
                return Auth.getTwoFactor().getDefaultResponse();
            }
            entityManager.getTransaction().begin();
            try {
                currencyId = currencyId.toUpperCase();
                Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(), currencyId, entityManager);
                Wallets walletModel = wallet.getWalletModel();
                if(walletModel.getBalance().subtract(walletModel.getLocked()).compareTo(amount) < 0) {
                    entityManager.getTransaction().rollback();
                    return new ErrorResponse("Brak wystarczających środków na rachunku");
                }
                wallet.lockFunds(amount);
                wallet.save();

                Withdrawals withdrawal = new Withdrawals();
                withdrawal.setAccountNumber(accountNumber);
                withdrawal.setAmount(amount);
                withdrawal.setTitle(title);
                withdrawal.setStatus("PENDING");
                withdrawal.setCurrencyId(currencyId);
                withdrawal.setUserId(Auth.getUser().getUserId());
                withdrawal.setCreated(new Date());
                entityManager.persist(withdrawal);
                entityManager.getTransaction().commit();
                return new SuccessResponse("Wypłata została dodana");


            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie udało się dodać wypłaty");
            }
        }
    }

    public static Boolean isAccountNumberValid(String currencyId, String input)
    {
        input = input.replace(" ", "");
        if(currencyId.equals("PLN")) {
            if (input.length() != 26 && input.length() != 32) {
                return false;
            }
            return true;
        }
        else if(currencyId.equals("BTC")) {
            if (input.length() < 26 && input.length() > 32) {
                return false;
            }
            return true;
        }
        return false;
    }

}
