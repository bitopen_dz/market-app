package org.bitopen.controllers;

import org.bitopen.models.Users;
import org.bitopen.models.UsersDetails;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.bitopen.utils.UserExtractor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dzach on 21.02.2017.
 */
@RestController
@RequestMapping(value="/admin/users")
public class UsersAdminController {

    private static EntityManagerFactory entityManagerFactory = null;
    private static EntityManager entityManager = null;

    @PostConstruct
    public void init() {
        _initEntityManager();
    }

    private static void _initEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public Response findByEmail(@RequestHeader(value = "Authorization") String token,
                         @RequestParam(value="email", required=false) String email) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else if (!Auth.getUser().isAdmin()) {
            return new ErrorResponse("Niewystarczające uprawnienia do wykonania akcji");
        } else {
            try {
                List users = entityManager.createNamedQuery("Users.emailLike")
                        .setParameter("email", "%"+email+"%")
                        .getResultList();

                return new SuccessResponse("Użytkownicy zostali pobrani", users);
            } catch (Exception e) {
                e.printStackTrace();
                return new ErrorResponse("Nie udało się pobrać użytkowników");
            }
        }
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public Response getSingle(@RequestHeader(value = "Authorization") String token,
                        @PathVariable Long userId) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else if (!Auth.getUser().isAdmin()) {
            return new ErrorResponse("Niewystarczające uprawnienia do wykonania akcji");
        } else {
            entityManager.getTransaction().begin();
            try {
                List users = entityManager.createNamedQuery("Users.findByUserId")
                        .setParameter("userId", userId)
                        .getResultList();
                Users user = (Users)users.get(0);

                List userDetailsList = entityManager.createNamedQuery("UsersDetails.findByUserId")
                        .setParameter("userId", userId)
                        .getResultList();
                HashMap<String, Object> response = new HashMap<>();

                UsersDetails userDetails = null;
                if(userDetailsList.size() > 0) {
                    userDetails = (UsersDetails) userDetailsList.get(0);
                }
                response.put("user", user);
                response.put("details", userDetails);


                entityManager.getTransaction().commit();
                return new SuccessResponse("Użytkownik został pobrany", response);
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie znaleziono wybranego użytkownika");
            }
        }

    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public Response update(@RequestHeader(value = "Authorization") String token,
                           @PathVariable Long userId,
                           @RequestBody UserExtractor newUserData) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else if (!Auth.getUser().isAdmin()) {
            return new ErrorResponse("Niewystarczające uprawnienia do wykonania akcji");
        } else {
            entityManager.getTransaction().begin();
            try {
                List users = entityManager.createNamedQuery("Users.findByUserId")
                        .setParameter("userId", userId)
                        .getResultList();
                Users user = (Users)users.get(0);

                if(newUserData.getEmail() != null) {
                    user.setEmail(newUserData.getEmail());
                }
                if(newUserData.getStatus() != null) {
                    user.setStatus(newUserData.getStatus());
                }
                if(newUserData.getPassword() != null) {
                    user.setPassword(newUserData.getPassword());
                }
                entityManager.merge(user);
                entityManager.getTransaction().commit();
                return new SuccessResponse("Dane użytkownika zostały zapisane", user);
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie udało się zapisać danych użytkownika");
            }
        }
    }


}
