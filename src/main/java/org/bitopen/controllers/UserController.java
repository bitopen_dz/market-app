/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.bitopen.models.Users;
import org.bitopen.models.UsersDetails;
import org.bitopen.models.UsersTwoFactor;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.bitopen.utils.OneTimePasswordSecret;
import org.bitopen.utils.UserDetailsExtractor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author dzach
 */
@RestController
public class UserController {

    private static EntityManagerFactory entityManagerFactory = null;
    private static EntityManager entityManager = null;

    @PostConstruct
    public void init() {
        _initEntityManager();
    }

    private static void _initEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @RequestMapping(value = "/users/twoFactor", method = RequestMethod.GET)
    public Response getTwoFactor(@RequestHeader(value = "Authorization") String token) {

        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            UsersTwoFactor currentTwoFactor = Auth.getTwoFactor().getCurrentUserTwoFactor();
            Boolean existance = currentTwoFactor != null;
            return new SuccessResponse("Pobrano status dwustopniowej weryfikacji", existance);
        }
    }

    @RequestMapping(value = "/users/twoFactor/disable", method = RequestMethod.POST)
    public Response deleteTwoFactor(@RequestHeader(value = "Authorization") String token,
                                    @RequestParam(value = "key", required = false) String twoFactorKey) {

        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {

            if (Auth.getTwoFactor().isRequiredAndInvalid("login", twoFactorKey)) {
                return Auth.getTwoFactor().getDefaultResponse();
            }
            entityManager.getTransaction().begin();
            UsersTwoFactor currentTwoFactor = Auth.getTwoFactor().getCurrentUserTwoFactor();
            if (currentTwoFactor != null) {
                removeCurrentTwoFactor(currentTwoFactor);
            }
            entityManager.getTransaction().commit();
            return new SuccessResponse("Dwustopniowa weryfikacja została wyłączona");
        }
    }

    @RequestMapping(value = "/users/twoFactor", method = RequestMethod.POST)
    public Response createTwoFactor(@RequestHeader(value = "Authorization") String token) {

        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            entityManager.getTransaction().begin();
            UsersTwoFactor currentTwoFactor = Auth.getTwoFactor().getCurrentUserTwoFactor();
            if (currentTwoFactor != null) {
                removeCurrentTwoFactor(currentTwoFactor);
            }

            String secret = createNewTwoFactor();

            entityManager.getTransaction().commit();
            return new SuccessResponse("Utworzono klucz dwustopniowej weryfikacji", secret);
        }
    }

    @RequestMapping(value = "/users/details", method = RequestMethod.GET)
    public Response getDetails(@RequestHeader(value = "Authorization") String token) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            UsersDetails currentUsersDetails = entityManager.find(UsersDetails.class,
                    Auth.getUser().getUserId());
            return new SuccessResponse("Dane użytkownika zostały pobrane", currentUsersDetails);
        }
    }

    @RequestMapping(value = "/users/details", method = RequestMethod.POST)
    public Response updateProfile(@RequestHeader(value = "Authorization") String token,
                                  @RequestBody UserDetailsExtractor userDetailsExtractor) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            if (!userDetailsExtractor.isValid()) {
                return new ErrorResponse("Wprowadzono nieprawidłowe dane.");
            }
            if (Auth.getTwoFactor().isRequiredAndInvalid("update_details", userDetailsExtractor.getTwoFactorKey())) {
                return Auth.getTwoFactor().getDefaultResponse();
            }
            entityManager.getTransaction().begin();
            UsersDetails currentUsersDetails = entityManager.find(UsersDetails.class,
                    Auth.getUser().getUserId());
            UsersDetails usersDetails = userDetailsExtractor.toUsersDetailsModel();
            try {
                if (currentUsersDetails != null) {
                    entityManager.merge(usersDetails);
                } else {
                    entityManager.persist(usersDetails);
                }
                entityManager.getTransaction().commit();
                return new SuccessResponse("Dane użytkownika zostały zaktualizowane");
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie udało się zaktualizować danych użytkownika");
            }
        }
    }

    @RequestMapping(value = "/users/changePassword", method = RequestMethod.POST)
    public Response changePassword(@RequestHeader(value = "Authorization") String token,
                                   @RequestParam(value = "password", required = false) String password,
                                   @RequestParam(value = "passwordRepeat", required = false) String passwordRepeat,
                                   @RequestParam(value = "key", required = false) String twoFactorKey) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            if (!password.equals(passwordRepeat)) {
                return new ErrorResponse("Hasła nie są identyczne.");
            }
            if (Auth.getTwoFactor().isRequiredAndInvalid("update_details", twoFactorKey)) {
                return Auth.getTwoFactor().getDefaultResponse();
            }
            entityManager.getTransaction().begin();
            Users user = Auth.getUser();
            user.setPassword(Auth.hashPassword(password));
            entityManager.merge(user);
            entityManager.getTransaction().commit();
            return new SuccessResponse("Hasło zostało zmienione");
        }
    }

    private String createNewTwoFactor() {
        OneTimePasswordSecret oneTimePasswordSecret = OneTimePasswordSecret.create();

        UsersTwoFactor newTwoFactor = new UsersTwoFactor();
        newTwoFactor.setSecretKey(oneTimePasswordSecret.getSecretKey());
        newTwoFactor.setUserId(Auth.getUser().getUserId());
        entityManager.merge(newTwoFactor);

        return oneTimePasswordSecret.getSecretKey();
    }

    private void removeCurrentTwoFactor(UsersTwoFactor currentTwoFactor) {
        UsersTwoFactor tempTwoFactor = entityManager.find(UsersTwoFactor.class, currentTwoFactor.getTwoFactorId());
        entityManager.remove(tempTwoFactor);
    }
}
