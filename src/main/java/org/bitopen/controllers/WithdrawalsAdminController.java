package org.bitopen.controllers;

import org.bitopen.models.Withdrawals;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dzach on 21.02.2017.
 */
@RestController
@RequestMapping(value = "/admin/withdrawals")
public class WithdrawalsAdminController {
    private String myAccountNumber = "85114020040000390276188820";
    private String myName = "Appteria Damian Zachwieja";
    private static EntityManagerFactory entityManagerFactory = null;
    private static EntityManager entityManager = null;

    @PostConstruct
    public void init() {
        _initEntityManager();
    }

    private static void _initEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
    }


    @RequestMapping(value = "/get/{currencyId}/{status}", method = RequestMethod.GET)
    public Response get(@RequestHeader(value = "Authorization") String token,
                        @PathVariable String currencyId,
                        @PathVariable String status) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else if (!Auth.getUser().isAdmin()) {
            return new ErrorResponse("Niewystarczające uprawnienia do wykonania akcji");
        } else {
            entityManager.getTransaction().begin();
            try {
                List withdrawals = entityManager.createNamedQuery("Withdrawals.findByCurrencyIdAndStatus")
                        .setParameter("currencyId", currencyId)
                        .setParameter("status", status)
                        .getResultList();

                entityManager.getTransaction().commit();
                return new SuccessResponse("Wypłaty zostały pobrane", withdrawals);
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie udało się pobrać wypłat");
            }
        }
    }

    @RequestMapping(value = "/status/{withdrawalId}", method = RequestMethod.POST)
    public Response setStatus(@RequestHeader(value = "Authorization") String token,
                              @PathVariable Integer withdrawalId,
                              @RequestParam(value = "status") String status) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else if (!Auth.getUser().isAdmin()) {
            return new ErrorResponse("Niewystarczające uprawnienia do wykonania akcji");
        } else {
            entityManager.getTransaction().begin();
            try {
                List withdrawals = entityManager.createNamedQuery("Withdrawals.findByWithdrawalId")
                        .setParameter("withdrawalId", withdrawalId)
                        .getResultList();
                Withdrawals withdrawal = (Withdrawals) withdrawals.get(0);
                if (withdrawal == null) {
                    return new ErrorResponse("Nie znaleziono wypłaty");
                }
                withdrawal.setStatus(status);
                entityManager.merge(withdrawal);
                entityManager.getTransaction().commit();
                return new SuccessResponse("Wypłaty zostały pobrane", withdrawals);
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie udało się zaktualizować wypłaty");
            }
        }
    }

    @RequestMapping(value = "/file/{token}/{withdrawalIds}", method = RequestMethod.GET)
    public Response getFile(@PathVariable String token,
                            @PathVariable String withdrawalIds,
                            HttpServletResponse response) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else if (!Auth.getUser().isAdmin()) {
            return new ErrorResponse("Niewystarczające uprawnienia do wykonania akcji");
        } else {
            entityManager.getTransaction().begin();
            try {
                ArrayList<Integer> ids = new ArrayList<>();
                String[] withdrawalsIdsArray = withdrawalIds.split(",");
                for (String currentId : withdrawalsIdsArray) {
                    ids.add(Integer.valueOf(currentId));
                }
                response.setContentType("text/csv");
                response.setCharacterEncoding("UTF-8");
                response.addHeader("Content-Disposition", "attachment; filename=przelewy.csv");

                List withdrawals = entityManager.createNamedQuery("Withdrawals.findByWithdrawalIds")
                        .setParameter("withdrawalIds", ids)
                        .getResultList();
                for (Object withdrawalObj : withdrawals) {
                    try {
                        Writer writer = response.getWriter();
                        writer.write(withdrawalToBankImportLine((Withdrawals) withdrawalObj));
                        //response.getOutputStream().flush();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }


                entityManager.getTransaction().commit();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                entityManager.getTransaction().rollback();
                return new ErrorResponse("Nie znaleziono wypłaty");
            }
        }
    }

    private String withdrawalToBankImportLine(Withdrawals withdrawal) {
        String[] fields = new String[16];
        fields[0] = "110";
        fields[1] = getTodayDate();
        fields[2] = withdrawal.getAmount().multiply(new BigDecimal(100)).toBigInteger().toString();
        fields[3] = getMySettleNumber();
        fields[4] = "0";
        fields[5] = "\""+getMyAccountNumber()+"\"";
        fields[6] = "\""+formatAccountNumber(withdrawal.getAccountNumber())+"\"";
        fields[7] = getMyName();
        fields[8] = formatRecipientName(withdrawal.getUserId());
        fields[9] = "0";
        fields[10] = getSettleNumberFromAccountNumber(withdrawal.getAccountNumber());
        fields[11] = "\"Wypłata " + withdrawal.getWithdrawalId() + "\"";
        fields[12] = "\"\"";
        fields[13] = "\"\"";
        fields[14] = "51";
        fields[15] = "";
        return String.join(",", fields);
    }

    private String formatRecipientName(Long userId) {
        return "\"Test|||\"";
    }

    private String formatAccountNumber(String accountNumber) {
        return accountNumber.replace(" ", "");
    }

    public String getTodayDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getMySettleNumber() {
        return getSettleNumberFromAccountNumber(myAccountNumber);
    }

    private String getSettleNumberFromAccountNumber(String accountNumber) {
        accountNumber = formatAccountNumber(accountNumber);
        return accountNumber.substring(2, 10);
    }

    public String getMyAccountNumber() {
        return myAccountNumber;
    }

    public String getMyName() {
        return "\"" + myName + "|||\"";
    }
}
