/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import org.bitopen.models.Currencies;
import org.bitopen.models.DepositsWatchlist;
import org.bitopen.types.*;
import org.bitopen.utils.Auth;
import org.bitopen.utils.Wallet;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author dzach
 */
@RestController
public class DepositsController {

    private static EntityManagerFactory factory = null;

    @PostConstruct
    public void init() {
        _initEntityManager();
    }

    private static void _initEntityManager() {
        factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
    }

    @RequestMapping(value = "/deposits/{currencyId}", method = RequestMethod.GET)
    public Response getByCurrency(@RequestHeader(value = "Authorization") String token,
                            @PathVariable String currencyId) {

        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            EntityManager em = factory.createEntityManager();
            Currencies c = em.find(Currencies.class, currencyId);
            if (c == null) {
                return new ErrorResponse("Nierozpoznana waluta " + currencyId + "");
            }
            List deposits = em.createNamedQuery("DepositsWatchlist.findByUserIdCurrency")
                    .setParameter("userId", Auth.getUser().getUserId())
                    .setParameter("currencyId", currencyId)
                    .getResultList();
            return new SuccessResponse("Wpłaty zostały poprawnie pobrane", deposits);
        }
    }

    @RequestMapping(value = "/deposits/{currencyId}/{status}", method = RequestMethod.GET)
    public Response getByStatus(@RequestHeader(value = "Authorization") String token,
                            @PathVariable String status,
                            @PathVariable String currencyId) {

        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        } else {
            EntityManager em = factory.createEntityManager();
            Currencies c = em.find(Currencies.class, currencyId);
            if (c == null) {
                return new ErrorResponse("Nierozpoznana waluta " + currencyId + "");
            }
                List deposits = em.createNamedQuery("DepositsWatchlist.findByUserIdCurrencyAndStatus")
                        .setParameter("userId", Auth.getUser().getUserId())
                        .setParameter("currencyId", currencyId)
                        .setParameter("status", status)
                        .getResultList();
                return new SuccessResponse("Wpłaty zostały poprawnie pobrane", deposits);
        }
    }

    @RequestMapping(value = "/deposits/dotpay/start", method = RequestMethod.POST)
    public Response startDotpay(@RequestHeader(value = "Authorization") String token,
                                @RequestParam("amount") BigDecimal amount) {
        if (!Auth.retreiveSession(token)) {
            return new InvalidSessionTokenResponse();
        }
        UUID control = UUID.randomUUID();
        Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(), "PLN", factory.createEntityManager());
        String form = "<form action='https://ssl.dotpay.pl/test_payment/' method='post' id='dotpay-form'>" +
                "<input type='hidden' name='id' value='" + DotpayStatus.ID.toString() + "' />" +
                "<input type='hidden' name='amount' value='" + amount.toString() + "' />" +
                "<input type='hidden' name='currency' value='PLN' />" +
                "<input type='hidden' name='type' value='0' />" +
                "<input type='hidden' name='description' value='Wpłata do serwisu BitOpen' />" +
                "<input type='hidden' name='control' value='" + control.toString() + "' />" +
                "<input type='hidden' name='URL' value='" +
                "http://bitopen.pl/#panel/wallet/fiat/id-" + wallet.getWalletModel().getWalletId() + "' />" +
                "</form><script>document.getElementById('dotpay-form').submit();</script>";

        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        DepositsWatchlist deposit = new DepositsWatchlist();
        deposit.setTxid(control.toString());
        deposit.setAmount(amount);
        deposit.setCurrencyId("PLN");
        deposit.setUserId(Auth.getUser().getUserId());
        deposit.setStatus("PENDING");
        deposit.setConfirmations(0);

        em.persist(deposit);
        em.getTransaction().commit();
        em.close();

        return new SuccessResponse("Płatność została prawidłowo rozpoczęta", form);
    }

    @RequestMapping(value = "/deposits/dotpay/status", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String statusDotpay(@RequestParam HashMap<String, String> params) {
        System.out.println("TEST");
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        DotpayStatus dotpayStatus = new DotpayStatus(params);
        System.out.println("Start");
        if (dotpayStatus.isValid()) {
            System.out.println("Valid");
            em.getEntityManagerFactory().getCache().evict(DepositsWatchlist.class, dotpayStatus.get("control"));
            DepositsWatchlist deposit = em.find(DepositsWatchlist.class, dotpayStatus.get("control"));
            if (deposit != null && deposit.getStatus().equals("PENDING")) {
                System.out.println("processing");
                deposit.setStatus("SUCCESS");
                deposit.setLastCheck(new Date());

                Wallet wallet = Wallet.byUser(deposit.getUserId(), deposit.getCurrencyId(), em);
                if (wallet != null) {
                    wallet.addFunds(deposit.getAmount(), "deposit");
                    wallet.save();
                }
            }
        }
        em.getTransaction().commit();
        em.close();
        return "OK";
    }
}
