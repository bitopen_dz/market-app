/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import org.bitopen.exceptions.InvalidEmailException;
import org.bitopen.exceptions.TwoFactorTokenExpected;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.Response;
import java.util.HashMap;
import javax.transaction.Transactional;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author dzach
 */
@RestController
@CrossOrigin(origins = "http://market.bitopen.pl")
public class AuthorizationController {
    
    @RequestMapping( value = "/auth/authorization" , method = RequestMethod.POST )
    public Response authorize( @RequestParam("email") String email,
                               @RequestParam("password") String password,
                               @RequestParam(value="key", required = false) String twoFactorKey) {

        try {
            if(Auth.authorize(email, password, twoFactorKey)){
                HashMap<String, Object> hm = new HashMap<String, Object>();
                hm.put("sessionKey", Auth.getSessionKey());
                return new SuccessResponse("Zalogowano poprawnie", hm);
            }
            else {
                return new ErrorResponse("Niewłaściwy login, hasło lub token");
            }
        } catch (TwoFactorTokenExpected twoFactorTokenExpected) {
            return new ErrorResponse("Wymagane podanie tokenu dwustopniowej weryfikacji", "2fa_token_required");
        }
    }
    
    
    @RequestMapping( value = "/auth/check" , method = RequestMethod.GET )
    public Response check(@RequestHeader(value="Authorization") String token){
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            HashMap<String, Object> hm = new HashMap<String, Object>();
            hm.put("user", Auth.getUser());
            return new SuccessResponse("Klucz sesji jest prawidłowy", hm);
        }
        
    }

    @RequestMapping( value = "/auth/register" , method = RequestMethod.POST )
    public Response register(@RequestParam("email") String email,
                             @RequestParam("password") String password,
                             @RequestParam("passwordRepeat") String passwordRepeat) {
        if(email.length() == 0 || password.length() == 0){
            return new ErrorResponse("Adres e-mail i hasło są wymagane");
        }
        if(!password.equals(passwordRepeat)){
            return new ErrorResponse("Hasła nie są identyczne");
        }
        try {
            Auth.create(email, password);
        }
        catch(InvalidEmailException e){
            return new ErrorResponse("Podano nieprawidłowy adres e-mail");
        }
        catch(Exception e){
            return new ErrorResponse("Wystąpił nieznany błąd, spróbuj ponownie");
        }

        return new SuccessResponse("Konto zostało poprawnie utworzone");
    }
    @RequestMapping( value = "/auth/activate" , method = RequestMethod.POST )
    public Response activate(@RequestParam("activationKey") String activationKey) {
        if(activationKey.length() == 0 ){
            return new ErrorResponse("Błędny klucz aktywacyjny");
        }
        try {
            Auth.activate(activationKey);
        }
        catch(Exception e){
            return new ErrorResponse("Wystąpił nieznany błąd, spróbuj ponownie");
        }

        return new SuccessResponse("Konto zostało poprawnie aktywowane");
    }

    @RequestMapping( value = "/auth/logout" , method = RequestMethod.GET )
    public Response logout(@RequestHeader(value="Authorization") String token) { 
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            Auth.logout(token);
            return new SuccessResponse("Wylogowano poprawnie");
        } 
    }
    
}
