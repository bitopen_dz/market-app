package org.bitopen.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bitopen.models.Orders;
import org.bitopen.models.Wallets;
import org.bitopen.models.daos.OrdersDao;
import org.bitopen.types.ExchangeOffer;
import org.bitopen.types.SocketMessage;
import org.bitopen.utils.Auth;
import org.bitopen.utils.SingleTransaction;
import org.bitopen.utils.Wallet;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WebsocketController extends TextWebSocketHandler {
    private final static List<WebSocketSession> sessions;

    static {
        sessions = Collections.synchronizedList(new ArrayList<>());
    }

    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    @PostConstruct
    public void init() {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        System.out.println("Connection estabilished");
        sessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        System.out.println("Connection closed");
        sessions.remove(session);
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) {
        if (!sessions.contains(session)) {
            sessions.add(session);
        }
        try {
            if(entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            entityManager.getTransaction().begin();

            Gson gson = new Gson();
            System.out.println(message.getPayload());
            ExchangeOffer offer = gson.fromJson(message.getPayload(), ExchangeOffer.class);
            System.out.println(offer.getType());
            Auth.retreiveSession(offer.getUserToken());
            ArrayList<SocketMessage> response = new ArrayList<>();
            if (offer.getType().equals("cancel")) {
                response.add(cancelOffer(offer));
            } else if (!areUsersBalancesValid(offer)) {
                response.add(getNotSufficientBalanceMessage());
            } else if (!isOfferRealizable(offer)) {
                response.add(insertOffer(offer));
            } else {
                response = realizeOffer(offer);
            }

            entityManager.getTransaction().commit();
            for (SocketMessage toSend : response) {
                sendMessage(toSend);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private SocketMessage cancelOffer(ExchangeOffer offer) {
        Orders order = entityManager.find(Orders.class, offer.getId());
        if (order == null || order.getUserId() != Auth.getUser().getUserId()) {
            return createNotificationMessage("Oferta nie została znaleziona. " +
                    "" + order.getUserId() + " " + Auth.getUser().getUserId());
        }
        if(order.getType().equals("bid")){
            Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(),
                    offer.getPairId().substring(3, 6),
                    entityManager);
            wallet.unlockFunds(order.getPrice());
            wallet.save();
        } else {
            Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(),
                    offer.getPairId().substring(0, 3),
                    entityManager);
            wallet.unlockFunds(order.getAmount());
            wallet.save();
        }
        order.setStatus("CANCELLED");
        order.setAmount(new BigDecimal(0));
        order.setPrice(new BigDecimal(0));
        OrdersDao.setEntityManager(entityManager);
        OrdersDao.save(order);
        SocketMessage message = new SocketMessage();
        message.setType("offer-changed");
        message.setContent(order);
        return message;
    }


    private void sendMessage(SocketMessage message) {
        Gson gson = new Gson();
        System.out.println("TEST");
        synchronized (sessions) {
            for (WebSocketSession session : sessions) {
                TextMessage response = new TextMessage(gson.toJson(message));
                try {
                    session.sendMessage(response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private ArrayList<SocketMessage> realizeOffer(ExchangeOffer offer) {
        List<Orders> orders = getOrdersMatchedTo(offer);
        ArrayList<SocketMessage> response = new ArrayList<>();
        for (Orders order : orders) {
            if (offer.getAmount().compareTo(OrdersDao.MINIMAL_AMOUNT) <= 0) {
                break;
            }
            SingleTransaction singleTransaction = new SingleTransaction();
            singleTransaction.setUserOffered(offer);
            singleTransaction.setCurrentOrder(order);
            singleTransaction.process();
            offer.setAmount(offer.getAmount().subtract(singleTransaction.getRealizedAmount()));
            response.add(singleTransaction.getSocketMessage());
            if (singleTransaction.transactionMade()) {
                response.add(singleTransaction.getTransactionMadeSocketMessage());
            }
        }
        if (offer.getAmount().compareTo(OrdersDao.MINIMAL_AMOUNT) > 0) {
            response.add(insertOffer(offer));
        }
        return response;
    }

    private SocketMessage insertOffer(ExchangeOffer offer) {
        Auth.retreiveSession(offer.getUserToken());
        Orders order = new Orders();
        order.setUserId(Auth.getUser().getUserId());
        order.setAmount(offer.getAmount());
        order.setPairId(offer.getPairId());
        order.setPrice(offer.getAmount().multiply(offer.getRate()));
        order.setRate(offer.getRate());
        order.setType(offer.getType());
        entityManager.persist(order);

        if (order.getType().equals("bid")) {
            lockFunds(offer.getAmount().multiply(offer.getRate()), offer.getPairId().substring(3, 6));
        } else {
            lockFunds(offer.getAmount(), offer.getPairId().substring(0, 3));
        }

        SocketMessage message = new SocketMessage();
        message.setContent(order);
        message.setType("new_order");

        return message;
    }

    private void lockFunds(BigDecimal amount, String currency) {
        Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(), currency, entityManager);
        wallet.lockFunds(amount);
        wallet.save();
    }


    private Boolean isOfferRealizable(ExchangeOffer offer) {
        List<Orders> orders = getOrdersMatchedTo(offer);
        System.out.println("found " + orders.size() + " offers");
        return orders.size() > 0;
    }

    private List<Orders> getOrdersMatchedTo(ExchangeOffer offer) {
        String rate = offer.getRate().toPlainString();
        switch (offer.getType()) {
            case "bid":
                return OrdersDao.conditionalGet("type = 'ask' AND rate <= " + rate + " " +
                                "AND status = 'ACTIVE'",
                        "rate ASC");
            case "ask":
                return OrdersDao.conditionalGet("type = 'bid' AND rate >= " + rate + " " +
                                "AND status = 'ACTIVE'",
                        "rate DESC");
        }
        return null;
    }

    private boolean areUsersBalancesValid(ExchangeOffer offer) {
        System.out.println(offer.getType());
        if (offer.getType().equals("bid")) {
            System.out.println("bid");
            Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(),
                    offer.getPairId().substring(3, 6),
                    entityManager);
            System.out.println(offer.getPairId().substring(3, 6));
            Wallets walletModel = wallet.getWalletModel();
            BigDecimal availableBalance = walletModel.getBalance().subtract(walletModel.getLocked());
            System.out.println(availableBalance);
            System.out.println(walletModel.getLocked());
            return availableBalance.compareTo(offer.getAmount().multiply(offer.getRate())) >= 0;
        } else {
            System.out.println("ask");
            Wallet wallet = Wallet.byUser(Auth.getUser().getUserId(),
                    offer.getPairId().substring(0, 3),
                    entityManager);
            System.out.println(offer.getPairId().substring(0, 3));
            Wallets walletModel = wallet.getWalletModel();
            BigDecimal availableBalance = walletModel.getBalance().subtract(walletModel.getLocked());
            System.out.println(availableBalance);
            System.out.println(walletModel.getLocked());
            return availableBalance.compareTo(offer.getAmount()) >= 0;
        }
    }

    private SocketMessage getNotSufficientBalanceMessage() {
        return createNotificationMessage("Niewystarczająca ilość środków na koncie");
    }

    private SocketMessage createNotificationMessage(String content) {
        SocketMessage message = new SocketMessage();
        message.setType("notification");
        message.setContent(content);
        return message;
    }

}