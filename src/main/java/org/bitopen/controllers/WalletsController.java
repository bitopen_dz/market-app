/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.bitopen.models.Currencies;
import org.bitopen.models.Users;
import org.bitopen.models.Wallets;
import org.bitopen.models.WalletsHistory;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.bitopen.utils.BitgoService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dzach
 */
@RestController
public class WalletsController {
    
    private static EntityManagerFactory factory = null;
    
    @PostConstruct
    public void init(){
        _initEntityManager();
    }

    @RequestMapping( value = "/wallets" , method = RequestMethod.GET )
    public Response get(@RequestHeader(value="Authorization") String token){
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            EntityManager em = factory.createEntityManager();
            factory.getCache().evict(Wallets.class);
            List<Wallets> wallets = em.createNamedQuery("Wallets.findByUserId")
                    .setParameter("userId", Auth.getUser().getUserId())
                    .getResultList();
            return new SuccessResponse("Portfele zostały poprawnie pobrane", wallets);
        }
    }
    @RequestMapping( value = "/wallets/id/{id}" , method = RequestMethod.GET )
    public Response getSingle(@RequestHeader(value="Authorization") String token,
                              @PathVariable Integer id){
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            EntityManager em = factory.createEntityManager();
            Wallets w = em.find(Wallets.class, id);
            if(w == null || Auth.getUser().getUserId() != w.getUserId()){
                return new ErrorResponse("Can't find specified wallet");
            }
            return new SuccessResponse("Portfel został poprawnie pobrany", w);
        }
    }


    @RequestMapping( value = "/wallets/history/{page}" , method = RequestMethod.GET )
    public Response history(@RequestHeader(value="Authorization") String token,
                            @PathVariable Integer page){
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            EntityManager em = factory.createEntityManager();

            List<Wallets> myWallets = em.createNamedQuery("Wallets.findByUserId")
                    .setParameter("userId", Auth.getUser().getUserId())
                    .getResultList();
            ArrayList<Integer> walletIds = new ArrayList<>();
            for (Wallets myWallet : myWallets) {
                walletIds.add(myWallet.getWalletId());
            }

            List<WalletsHistory> wallets = em.createNamedQuery("WalletsHistory.findByWalletIds")
                    .setParameter("walletIds", walletIds)
                    .setFirstResult((page-1) * 25)
                    .setMaxResults(25)
                    .getResultList();
            return new SuccessResponse("Historia operacji została poprawnie pobrana", wallets);
        }
    }

    @RequestMapping( value = "/wallets/historyCount" , method = RequestMethod.GET )
    public Response historyCount(@RequestHeader(value="Authorization") String token){
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            EntityManager em = factory.createEntityManager();

            List<Wallets> myWallets = em.createNamedQuery("Wallets.findByUserId")
                    .setParameter("userId", Auth.getUser().getUserId())
                    .getResultList();
            ArrayList<Integer> walletIds = new ArrayList<>();
            for (Wallets myWallet : myWallets) {
                walletIds.add(myWallet.getWalletId());
            }

            Double count = ((Number)em.createNamedQuery("WalletsHistory.countByWalletIds")
                    .setParameter("walletIds", walletIds)
                    .getSingleResult()).doubleValue();
            return new SuccessResponse("Ilość stron w historii została poprawnie pobrana",
                    (count / 25.0));
        }
    }

    @RequestMapping( value = "/wallets/{currencyId}" , method = RequestMethod.POST )
    public Response post(@RequestHeader(value="Authorization") String token,
                         @PathVariable String currencyId){
        
        if(!Auth.retreiveSession(token)){
            return new InvalidSessionTokenResponse();
        }
        else {
            
            if(!_currencyExists(currencyId)){
                return new ErrorResponse("Nierozpoznana waluta");
            }
            if(_walletExists(currencyId)){
                return new ErrorResponse("Portfel już istnieje w systemie");
            }
            else{
                Wallets wallet = _createWallet(currencyId);
                return new SuccessResponse("Portfel został poprawnie utworzony", wallet);
            }
            
        } 
    }
    
    private Boolean _walletExists(String currencyId){
        EntityManager em = factory.createEntityManager();
        List<Wallets> wallets = em.createNamedQuery("Wallets.findByUserAndCurrency")
                                .setParameter("userId", Auth.getUser().getUserId())
                                .setParameter("currencyId", currencyId)
                                .getResultList();
        return wallets.size() > 0;
    }
    
    private Boolean _currencyExists(String currencyId){
        EntityManager em = factory.createEntityManager();
        List<Currencies> currency = em.createNamedQuery("Currencies.findByCurrencyId")
                                .setParameter("currencyId", currencyId)
                                .getResultList();
        return currency.size() > 0;
    }
    
    
    
    private Wallets _createWallet(String currencyId){
        EntityManager em = factory.createEntityManager();
        List<Currencies> currencyL = em.createNamedQuery("Currencies.findByCurrencyId")
                                .setParameter("currencyId", currencyId)
                                .getResultList();
        if(currencyL.size() > 0){
            Currencies currency = currencyL.get(0);
            System.out.println(currency.isCrypto());
            if(currency.isCrypto()){
                return _createCryptoWallet(currencyId);
            }
            else{
                return _createFiatWallet(currencyId);
            }
        }
        return null;
    }
    
    private Wallets _createFiatWallet(String currencyId){
        System.out.println("creating fiat wallet");
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Wallets w = new Wallets(currencyId, Auth.getUser().getUserId());
        em.persist(w);
        em.getTransaction().commit();
        em.close();
        return w;
    }
    private Wallets _createCryptoWallet(String currencyId){
        System.out.println("creating crypto wallet");
        String address = _getBitcoinAddress();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Wallets w = new Wallets(address, currencyId, Auth.getUser().getUserId());
        em.persist(w);
        em.getTransaction().commit();
        em.close();
        return w;
    }
    
    private String _getBitcoinAddress(){
        System.out.println("BITGO GBA");
        BitgoService bgs = new BitgoService();
        return bgs.getNewAddress();
        //return "";
    }
    
    private static void _initEntityManager(){
        factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
    }
    
    
}
