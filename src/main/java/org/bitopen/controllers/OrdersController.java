/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import org.bitopen.models.Currencies;
import org.bitopen.models.DepositsWatchlist;
import org.bitopen.models.Orders;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.InvalidSessionTokenResponse;
import org.bitopen.types.Response;
import org.bitopen.types.SuccessResponse;
import org.bitopen.utils.Auth;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author dzach
 */
@RestController
public class OrdersController {


    private static EntityManagerFactory factory = null;

    @PostConstruct
    public void init(){
        _initEntityManager();
    }

    private static void _initEntityManager(){
        factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
    }

    @RequestMapping( value = "/orders/orderbook/{currencyPairId}" , method = RequestMethod.GET )
    public Response orderbook(@PathVariable String currencyPairId){
        EntityManager entityManager = factory.createEntityManager();

        List<Orders> bidOrders = entityManager
                .createNamedQuery("Orders.findActiveByPairIdAndTypeDESC")
                .setParameter("pairId", currencyPairId)
                .setParameter("type", "bid")
                .getResultList();
        List<Orders> askOrders = entityManager
                .createNamedQuery("Orders.findActiveByPairIdAndTypeASC")
                .setParameter("pairId", currencyPairId)
                .setParameter("type", "ask")
                .getResultList();
        entityManager.close();

        HashMap<String, List<Orders>> result = new HashMap<>();
        result.put("bid", bidOrders);
        result.put("ask", askOrders);

        return new SuccessResponse("Lista ofert została poprawnie pobrana", result);
    }
    @RequestMapping( value = "/orders/transactions/{currencyPairId}" , method = RequestMethod.GET )
    public Response transactions(@PathVariable String currencyPairId){
        EntityManager entityManager = factory.createEntityManager();

        List transactions = entityManager
                .createNamedQuery("Transactions.findLastByPairId")
                .setParameter("pairId", currencyPairId)
                .setMaxResults(100)
                .getResultList();

        entityManager.close();


        return new SuccessResponse("Lista transakcji została poprawnie pobrana", transactions);
    }

}
