/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.controllers;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.bitopen.models.DepositsWatchlist;
import org.bitopen.types.BitGoWebhook;
import org.bitopen.types.Transaction;
import org.bitopen.utils.BitgoService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dzach
 */
@RestController
public class WebhookController {
    
    
    private static EntityManagerFactory factory = null;
    
    @PostConstruct
    void init(){
      factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
    }
    
    @RequestMapping( value = "/bitGoNotification" , method = RequestMethod.POST )
    public Boolean notificationBitGo(@RequestBody BitGoWebhook webhook) {
        System.out.println("invome notification");
        String currencyId = "BTC";
        String txid = webhook.getHash();
        if(!_isValidTxid(txid) || _transactionExists(txid))
        {
            System.out.println("invalid notification");
            return false;
        }
        
        _addTxidToWatchList(currencyId, txid);
        return true;
    }
    private Boolean _isValidTxid(String txid){
        return txid.length() > 10;
    }
    
    private Boolean _transactionExists(String txid){
        
        EntityManager em = factory.createEntityManager();
        DepositsWatchlist tw = em.find(DepositsWatchlist.class, txid);
        em.close();
        if(tw == null){
            return false;
        }
        return true;
    }
    
    private void _addTxidToWatchList(String currencyId, String txid){
        System.out.println("adding to watchlist");
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        DepositsWatchlist tw = new DepositsWatchlist(txid, currencyId);
        
        try {
            em.persist(tw);
        }
        catch(Exception e){
            System.out.println(e);
        }
        em.getTransaction().commit();
        em.close();
    }
}
