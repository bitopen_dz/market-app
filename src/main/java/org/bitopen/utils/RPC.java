/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.net.URLEncoder;
import java.util.Random;
import org.apache.tomcat.util.codec.binary.Base64;
import org.bitopen.exceptions.RPCInvalidResponseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
//import org.json.JSONObject;
/**
 *
 * @author Damian
 */
public class RPC {
    
    private String protocol = "http";
    private String username;
    private String password;
    private String host;
    private String port;
    private HttpHeaders headers;
    
    private String[] params;
    
    private Integer currentId;
    
    private RestTemplate restTemplate;
    
    public RPC(String host){
        restTemplate = new RestTemplate();
        this.host = host;
        _initHeaders();
    }
    
    public RPC(String host, String port){
        restTemplate = new RestTemplate();
        this.host = host;
        this.port = port;
        _initHeaders();
    }
    
    public RPC(String host, String port, String username, String password){
        restTemplate = new RestTemplate();
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        _initHeaders();
    }
    
    public void setParams(String[] params){
       this.params = params;
    }
    
    public void addHeader(String name, String value){
       this.headers.add(name, value);
    }
    
    public RPCResponse call(String method) 
            throws RPCInvalidResponseException {
        String json = _getJson(method);
        System.out.println(_getUrl());
        System.out.println(json);
        HttpEntity<String> request = new HttpEntity<String>(json, headers);
        RPCResponse resp = restTemplate.postForObject(_getUrl(), request, RPCResponse.class);
        
        if(!checkCurrentId(resp.getId())){
            throw new RPCInvalidResponseException();
        }
        return resp;
    }
    
    public RPCResponse call(String method, String[] params) 
            throws Exception {
        this.params = params;
        return call(method);
    }
    
    private void _initHeaders(){
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        if(username.length() > 0 && password.length() > 0){
            System.out.println("TEST");
            String plainCreds = username+":"+password;
            byte[] plainCredsBytes = plainCreds.getBytes();
            byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
            String base64Creds = new String(base64CredsBytes);
            headers.add("Authorization", "Basic " + base64Creds);
        }
    }
    
    private String _getUrl(){
        return  (protocol + "://"
                + host + ":"
                + port + "/");
    }
    
    private String _getJson(String method){
        String json = "";
        Integer id = getCurrentId();
        
        if(params == null) {
            json = "{\"method\":\""+method+"\", \"id\": \""+id.toString()+"\"}";
        }
        else{
            String[] newParams = new String[ this.params.length ];
            Integer i = 0;

            for(String s : this.params){
                newParams[i] = "\""+s.replace("\"","'")+"\"";
                i++;
            }
            json = "{\"method\":\""+method+"\", \"id\": \""+id.toString()+"\", \"params\": ["+String.join(",", newParams)+"]}";
        }
        
        
        
        return json;
    }
    
    private Integer getCurrentId(){
        Random generator = new Random(); 
        Integer i = generator.nextInt(999) + 1;
        this.currentId = i;
        return i;
    }
    
    private Boolean checkCurrentId(Integer id){
        return id.equals(currentId);
    }
    
}
