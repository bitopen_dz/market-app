package org.bitopen.utils;

import org.bitopen.models.UsersDetails;

/**
 * Created by dzach on 22.01.2017.
 */
public class UserDetailsExtractor {

    private String firstname;
    private String lastname;
    private String street1;
    private String street2;
    private String city;
    private String country;
    private String postcode;
    private String idcardNumber;
    private String key;

    public boolean isValid() {
        return isNotEmpty(firstname)
                && isNotEmpty(lastname)
                && isNotEmpty(street1)
                && isNotEmpty(city)
                && isNotEmpty(country)
                && isNotEmpty(postcode)
                && isNotEmpty(idcardNumber);
    }

    public String getTwoFactorKey() {
        return key;
    }

    public UsersDetails toUsersDetailsModel() {
        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setUserId(Auth.getUser().getUserId());
        usersDetails.setFirstname(firstname);
        usersDetails.setLastname(lastname);
        usersDetails.setStreet1(street1);
        usersDetails.setStreet2(street2);
        usersDetails.setCountry(country);
        usersDetails.setPostcode(postcode);
        usersDetails.setIdcardNumber(idcardNumber);
        usersDetails.setCity(city);

        return usersDetails;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getIdcardNumber() {
        return idcardNumber;
    }

    public void setIdcardNumber(String idcardNumber) {
        this.idcardNumber = idcardNumber;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private boolean isNotEmpty(String item) {
        System.out.println(item);
        return item != null && item.length() > 0;
    }
}
