package org.bitopen.utils;

import org.bitopen.models.Users;
import org.bitopen.models.UsersTwoFactor;
import org.bitopen.types.ErrorResponse;
import org.bitopen.types.Response;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;

/**
 * Created by dzach on 14.01.2017.
 */
public class TwoFactor {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;
    private UsersTwoFactor currentUserTwoFactor;

    private Users currentUser;

    private TwoFactor(Users user) {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
        currentUser = user;
        loadTwoFactorObject();
    }

    private void loadTwoFactorObject() {
        List userTwoFactorList = entityManager.createNamedQuery("UsersTwoFactor.findByUserId")
                .setParameter("userId", currentUser.getUserId())
                .getResultList();
        if(userTwoFactorList.size() > 0) {
            currentUserTwoFactor = (UsersTwoFactor) userTwoFactorList.get(0);
        } else {
            currentUserTwoFactor = null;
        }
    }

    public UsersTwoFactor getCurrentUserTwoFactor() {
        return currentUserTwoFactor;
    }

    public static TwoFactor forUser(Users user) {
        return new TwoFactor(user);
    }

    public boolean isEnabledForAction(String action) {
        return currentUserTwoFactor != null;
    }

    public boolean isValidToken(String token) {
        System.out.println(token);
        if(token == null) {
            return false;
        }
        OneTimePasswordSecret oneTimePasswordSecret = OneTimePasswordSecret.fromString(currentUserTwoFactor.getSecretKey());
        try {
            return oneTimePasswordSecret.checkCode(Long.valueOf(token));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isRequiredAndInvalid(String action, String token) {
        System.out.println(action);
        System.out.println(token);
        System.out.println(this.isEnabledForAction(action));
        System.out.println(!this.isValidToken(token));
        return this.isEnabledForAction(action)
                 && !this.isValidToken(token);
    }

    public Response getDefaultResponse() {
        return new ErrorResponse("Wymagane podanie tokenu dwustopniowej weryfikacji", "2fa_token_required");
    }
}
