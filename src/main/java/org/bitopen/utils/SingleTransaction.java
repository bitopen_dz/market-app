package org.bitopen.utils;

import org.bitopen.models.Orders;
import org.bitopen.models.Transactions;
import org.bitopen.models.daos.OrdersDao;
import org.bitopen.types.ExchangeOffer;
import org.bitopen.types.SocketMessage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by zachmian on 30.12.16.
 */
public class SingleTransaction {
    private Orders currentOrder;
    private ExchangeOffer userOffered;
    private BigDecimal realizedAmount;
    private SocketMessage socketMessage;
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;
    private boolean transactionMade;
    private SocketMessage transactionMadeSocketMessage;

    public SingleTransaction() {
        entityManagerFactory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        entityManager = entityManagerFactory.createEntityManager();
        OrdersDao.setEntityManager(entityManager);
    }

    public void process() {
        entityManager.getTransaction().begin();
        if (currentOrder == null || userOffered == null) {
            return;
        }

        if (isUserOfferedGreaterThanCurrentOrder()) {
            processOfferWhole();
        } else {
            processOfferPartial();
        }
        transactionMade = true;
        entityManager.getTransaction().commit();
    }

    private void processOfferWhole() {
        BigDecimal price = currentOrder.getPrice();
        BigDecimal amount = currentOrder.getAmount();
        processOffer(price, amount);

        socketMessage = new SocketMessage();
        socketMessage.setType("offer-expired");
        socketMessage.setContent(currentOrder);
        realizedAmount = amount;
    }

    private void processOfferPartial() {
        BigDecimal amount = userOffered.getAmount();
        BigDecimal price = userOffered.getAmount().multiply(currentOrder.getRate());
        processOffer(price, amount);

        socketMessage = new SocketMessage();
        socketMessage.setType("offer-changed");
        socketMessage.setContent(currentOrder);
        realizedAmount = amount;
    }

    private void processOffer(BigDecimal price, BigDecimal amount) {
        Long buyerUserId = determineWhoIsBuying();
        Long sellerUserId = determineWhoIsSelling();
        moveFiatFunds(buyerUserId, sellerUserId, price);
        moveCryptoFunds(sellerUserId, buyerUserId, amount);
        updateCurrentOrder(amount);
        addTransactionEntry(price, amount, buyerUserId, sellerUserId);
    }

    private void addTransactionEntry(BigDecimal price, BigDecimal amount, Long buyerId, Long sellerId) {
        Transactions transaction = new Transactions();
        transaction.setAmount(amount);
        transaction.setPrice(price);
        transaction.setBuyerId(buyerId);
        transaction.setSellerId(sellerId);
        transaction.setOrderId(currentOrder.getOrderId());
        transaction.setCreated(new Date());
        transaction.setPairId(currentOrder.getPairId());

        entityManager.persist(transaction);
        transactionMadeSocketMessage = new SocketMessage();
        transactionMadeSocketMessage.setType("new-transaction");
        transactionMadeSocketMessage.setContent(transaction);
    }

    private void updateCurrentOrder(BigDecimal toSub) {
        currentOrder.setAmount(currentOrder.getAmount().subtract(toSub));
        currentOrder.setPrice(currentOrder.getAmount().multiply(currentOrder.getRate()));
        if (currentOrder.getAmount().compareTo(OrdersDao.MINIMAL_AMOUNT) <= 0) {
            currentOrder.setStatus("EXPIRED");
        }
        entityManager.merge(currentOrder);
        //OrdersDao.save(currentOrder);
    }

    private void moveFiatFunds(Long from, Long to, BigDecimal amount) {
        System.out.println("Substrning: " + currentOrder.getPairId().substring(3, 6));
        Wallet fromWallet = Wallet.byUser(from, currentOrder.getPairId().substring(3, 6), entityManager);
        Wallet toWallet = Wallet.byUser(to, currentOrder.getPairId().substring(3, 6), entityManager);
        if (currentOrder.getType().equals("bid")) {
            fromWallet.unlockFunds(amount);
        }
        fromWallet.subFunds(amount, "currency_tranasction");
        toWallet.addFunds(amount, "currency_tranasction");
        fromWallet.save();
        toWallet.save();
    }

    private void moveCryptoFunds(Long from, Long to, BigDecimal amount) {
        System.out.println("Substrning: " + currentOrder.getPairId().substring(0, 3));
        Wallet fromWallet = Wallet.byUser(from, currentOrder.getPairId().substring(0, 3), entityManager);
        Wallet toWallet = Wallet.byUser(to, currentOrder.getPairId().substring(0, 3), entityManager);
        if (currentOrder.getType().equals("ask")) {
            fromWallet.unlockFunds(amount);
        }
        fromWallet.subFunds(amount, "currency_tranasction");
        toWallet.addFunds(amount, "currency_tranasction");
        fromWallet.save();
        toWallet.save();
    }

    private Long determineWhoIsBuying() {
        if (userOffered.getType().equals("bid")) {
            return Auth.getUser().getUserId();
        } else {
            return currentOrder.getUserId();
        }
    }

    private Long determineWhoIsSelling() {
        if (userOffered.getType().equals("ask")) {
            return Auth.getUser().getUserId();
        } else {
            return currentOrder.getUserId();
        }
    }

    private boolean isUserOfferedGreaterThanCurrentOrder() {
        return userOffered.getAmount().compareTo(currentOrder.getAmount()) >= 0;
    }

    public void setCurrentOrder(Orders currentOrder) {
        this.currentOrder = currentOrder;
    }

    public void setUserOffered(ExchangeOffer userOffered) {
        this.userOffered = userOffered;
    }

    public BigDecimal getRealizedAmount() {
        return realizedAmount;
    }

    public SocketMessage getSocketMessage() {
        return socketMessage;
    }

    public boolean transactionMade() {
        return transactionMade;
    }

    public SocketMessage getTransactionMadeSocketMessage() {
        return transactionMadeSocketMessage;
    }
}
