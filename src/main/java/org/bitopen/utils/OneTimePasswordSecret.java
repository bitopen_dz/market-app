package org.bitopen.utils;
import org.apache.commons.codec.binary.Base32;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
/**
 * Created by dzach on 14.01.2017.
 */
public class OneTimePasswordSecret {
    private static final Random RAND = new Random();

    private Integer acceptedPastPeriods = 2;
    private Integer secretSize = 10;
    private String secretKey;

    private OneTimePasswordSecret() {
    }

    private OneTimePasswordSecret(String secretKey) {
        this.secretKey = secretKey;
    }

    public static OneTimePasswordSecret fromString(String secretKey) {
        return new OneTimePasswordSecret(secretKey);
    }

    public static OneTimePasswordSecret create() {
        OneTimePasswordSecret secret = new OneTimePasswordSecret();
        secret.setSecretKey(secret.getNewSecretKey());
        return secret;
    }

    public Integer getSecretSize() {
        return secretSize;
    }

    public void setSecretSize(Integer secretSize) {
        this.secretSize = secretSize;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public boolean checkCode(long code) throws NoSuchAlgorithmException, InvalidKeyException {
        OneTimePassword oneTimePassword = new OneTimePassword(this);
        for (int i = -acceptedPastPeriods; i <= acceptedPastPeriods; ++i) {
            long hash = oneTimePassword.generatePasswordForRelativePeriod(i);
            if (hash == code) {
                return true;
            }
        }
        return false;
    }

    private String getNewSecretKey() {
        byte[] buffer = allocateBuffer();
        return generateSecret(buffer);
    }

    private String generateSecret(byte[] buffer) {
        Base32 codec = new Base32();
        byte[] secretKey = Arrays.copyOf(buffer, secretSize);
        byte[] encodedKey = codec.encode(secretKey);
        return new String(encodedKey);
    }

    private byte[] allocateBuffer() {
        byte[] buffer = new byte[secretSize];
        RAND.nextBytes(buffer);
        return buffer;
    }

}