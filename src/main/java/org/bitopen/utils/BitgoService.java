/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import org.bitopen.types.Transaction;
import org.bitopen.types.TransactionOutput;
import org.springframework.http.HttpMethod;

/**
 * @author Damian
 */
public class BitgoService extends DefaultService {
    String WALLETID;

    public BitgoService() {
        MINIMUM_CONFIRMATIONS = 1;
        PROTOCOL = "https";
        USERNAME = "v2xe9a021329d5aca5df3c5d4667d37a4cb1ec52934ac117563d46d04574a6a9e06";
        HOST = "test.bitgo.com/api/v1";
        WALLETID = "2N5MnZRqjps3xWvMdrStNVu7W1FoQ4FWxMb";
    }

    public String getNewAddress() {
        System.out.println("GETTING NEW ADDRESS");
        HashMap<String, Object> response = _request("wallet",
                new String[]{WALLETID, "address", "1"},
                "POST");
        System.out.println("newaddress " + WALLETID);
        System.out.println(response);
        return (String) response.get("address");

    }

    @Override
    public Transaction getTransaction(String txid) {
        System.out.println(txid);
        HashMap<String, Object> response = _request("tx", new String[]{txid});
        System.out.println(response);

        Transaction tx = new Transaction((String) response.get("id"),
                (Integer) response.get("confirmations"));

        ArrayList details = (ArrayList) response.get("outputs");
        TransactionOutput[] txout = new TransactionOutput[details.size()];
        Integer i = 0;

        for (Object item : details) {
            HashMap<String, Object> _item = (HashMap<String, Object>) item;
            BigDecimal amount = null;
            try {
                amount = BigDecimal.valueOf((Long) _item.get("value"));
            } catch (ClassCastException e) {
                amount = BigDecimal.valueOf((Integer) _item.get("value"));
            }
            // sprowadzanie do wartości podstawowej
            amount = amount.divide(BigDecimal.valueOf(100000000));
            txout[i] = new TransactionOutput((String) _item.get("account"),
                    amount);
            i++;
        }

        tx.setOutputs(txout);
        return tx;
    }

    private String _getUrl(String method) {
        return PROTOCOL + "://"
                + HOST + "/"
                + method;
    }

    private HashMap<String, Object> _request(String method, String[] params) {
        return _request(method, params, "GET");
    }

    private HashMap<String, Object> _request(String method, String[] params, String httpMethod) {
        Request r = new Request();
        r.setHeader("Authorization", "Bearer " + USERNAME);
        System.out.println("tesT");
        //try{
        method = method + '/' + String.join("/", params);
        HashMap<String, Object> resp;
        switch (httpMethod) {
            case "POST":
                resp = r.post(_getUrl(method), HashMap.class, null);
                break;
            default:
                resp = r.get(_getUrl(method), HashMap.class, null);
                break;
        }
        System.out.println("hallelujah");
        System.out.println(resp);
        return resp;
        // }
        //catch(Exception e){

        // }

        // return null;
    }

}
