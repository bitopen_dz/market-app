/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
//import org.springframework.http.converter.

/**
 *
 * @author Damian
 */
public class Request {
    
    HttpHeaders headers;
    
    private RestTemplate restTemplate;
    
    public Request(){
        restTemplate = new RestTemplate();
        headers = new HttpHeaders();
    }
    
    public void setHeader(String name, String value){
        headers.set(name, value);
    }
    
    public <T> T get(String url, Class<T> responseType, HashMap<String, String> params){
        url = _addParamsToUrl(url, params);
        HttpEntity entity = new HttpEntity(headers);
        System.out.println(url);
        ResponseEntity<T> re = restTemplate.exchange(url, HttpMethod.GET, entity, responseType);
        return re.getBody();
    }
    
    public <T> T post(String url, Class<T> responseType, HashMap<String, Object> params){
        
        HttpEntity entity = new HttpEntity(headers);
        System.out.println(url);
        ResponseEntity<T> re = restTemplate.exchange(url, HttpMethod.POST, entity, responseType);
        return re.getBody();
        
    }
    
    
    public void delete(String url, List<?> params){
        restTemplate.delete(url, params);
    }
    
    public void put(String url, HashMap<String, Object> params){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        MultiValueMap<String, Object> map = _hashMapToMultiValueMap(params);
        
        String requestJson = "";
        HttpEntity<String> request = new HttpEntity<String>(requestJson, headers);

        restTemplate.put(url, request);
    
    }
    
    private MultiValueMap<String, Object> _hashMapToMultiValueMap(HashMap<String, Object> hashMap){
        LinkedMultiValueMap<String, Object> lmv = new LinkedMultiValueMap<String, Object>();
        Iterator it = hashMap.entrySet().iterator();
        
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            /*if(pair.getValue() instanceof HashMap){
                MultiValueMap<String, Object> val = _hashMapToMultiValueMap((HashMap<String, Object>)pair.getValue());
                lmv.add(String.valueOf(pair.getKey()), val);
            }
            else{*/
                lmv.add(String.valueOf(pair.getKey()), pair.getValue());
            //}
            it.remove(); // avoids a ConcurrentModificationException
        }
        return lmv;
    }
   
    private String _addParamsToUrl(String url, HashMap<String, String> hashMap){
        if(hashMap == null){
            return url;
        }
        if(!url.contains("?")){
            url = url + "?";
        }
        else{
            url = url + "&";
        }
        Iterator it = hashMap.entrySet().iterator();
        List<String> paramsArr = new ArrayList<String>();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            paramsArr.add(String.valueOf(pair.getKey())+ "=" +String.valueOf(pair.getValue()));
            it.remove(); 
        }
        System.out.println("Calling "+ url + String.join("&", paramsArr));
        return url + String.join("&", paramsArr);
    }
    
    
    
}
