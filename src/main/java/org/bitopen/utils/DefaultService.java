/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bitopen.exceptions.RPCInvalidResponseException;
import org.bitopen.types.Transaction;
import org.bitopen.types.TransactionOutput;

/**
 *
 * @author Damian
 */
public class DefaultService  {
    
    public Integer MINIMUM_CONFIRMATIONS;
    public String PROTOCOL;
    public String HOST;
    public String PORT;
    public String USERNAME;
    public String PASSWORD;
    
    public DefaultService(){}
    

    public Integer getMinimumConfirmations() {
        return this.MINIMUM_CONFIRMATIONS;
    }
    
    public List<Transaction> getTransactions(String address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Transaction getTransaction(String txid) {
        RPCResponse rpc = _request("gettransaction", new String[] {txid});
        HashMap<String, Object> hm = (HashMap<String, Object>) rpc.getResult();
        
        System.out.println(rpc.getResult());
        
        Transaction tx = new Transaction((String)hm.get("txid"),
                                         (Integer)hm.get("confirmations"),
                                         (Integer)hm.get("time"));
        
        ArrayList details = (ArrayList) hm.get("details");
        TransactionOutput[] txout = new TransactionOutput[ details.size() ];
        Integer i = 0;
        
        for(Object item : details){
            HashMap<String, Object> _item = (HashMap<String, Object>) item;
            txout[i] = new TransactionOutput((String)_item.get("address"), 
                                             BigDecimal.valueOf((Double)_item.get("amount")));
            i++;
        }
        
        tx.setOutputs(txout);
        
        return tx;
    }

    public String send(String address, BigDecimal amount, String comment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private RPCResponse _request(String method){
        return _request(method, null);
    }
    private RPCResponse _request(String method, String[] params){
        RPC r = new RPC(HOST, PORT, USERNAME, PASSWORD);
        r.setParams(params);
        try{
            RPCResponse resp = r.call(method);
            return resp;
        }
        catch(RPCInvalidResponseException e){ System.out.println("error");} 
        
        return null;
    }
    
}
