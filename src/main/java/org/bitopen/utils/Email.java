package org.bitopen.utils;


import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;

/**
 * Created by zachmian on 20.12.16.
 */
public class Email {


    private String title;
    private String to;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void send(){
        Configuration configuration = new Configuration()
                .domain("bitopen.pl")
                .apiKey("key-5b4dff06173f2c377d183bb8930207fb")
                .from("BitOpen", "no-reply@bitopen.pl");
        Mail.using(configuration)
                .to(to)
                .subject(title)
                .html(content)
                .build()
                .send();
    }
}
