/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.bitopen.models.Wallets;
import org.bitopen.models.WalletsHistory;

/**
 *
 * @author dzach
 */
public class Wallet {
    
    private static EntityManagerFactory factory = null;
    private Wallets walletModel;
    private EntityManager em;
    
    public Wallet(Long userId, String currencyId, EntityManager em){
        factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        this.em = em;
        this.walletModel = _getWallet(userId, currencyId);
    }
    public Wallet(String address, EntityManager em) {
        factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
        this.em = em;
        this.walletModel = _getWallet(address);
    }
    
    public static Wallet byAddress(String address, EntityManager em) {
        return new Wallet(address, em);
    }
    public static Wallet byUser(Long userId, String currencyId, EntityManager em) {
        return new Wallet(userId, currencyId, em);
    }
    
    public void setEntityManager(EntityManager em){
        this.em = em;
    }
    
    public Wallets getWalletModel(){
        return walletModel;
    }
    
    public void addFunds(BigDecimal amount, String type){
        System.out.println("Adding funds??? "+amount.toString());
        System.out.println(walletModel.getBalance().add(amount));
        walletModel.setBalance(walletModel.getBalance().add(amount));
        System.out.println("XXXXXXX");
        _createHistoryEntry(amount, walletModel, type);
    }
    public void subFunds(BigDecimal amount, String type){
        walletModel.setBalance(walletModel.getBalance().subtract(amount));
        _createHistoryEntry(amount.multiply(new BigDecimal(-1)), walletModel, type);
    }
    public void lockFunds(BigDecimal amount){
        walletModel.setLocked(walletModel.getLocked().add(amount));
    }
    public void unlockFunds(BigDecimal amount){
        walletModel.setLocked(walletModel.getLocked().subtract(amount));
    }
    
    private Wallets _getWallet(Long userId, String currencyId){
        Wallets w = (Wallets) em.createNamedQuery("Wallets.findByUserAndCurrency")
                                .setParameter("userId", userId)
                                .setParameter("currencyId", currencyId)
                                .getSingleResult();
        //em.close();
        factory.getCache().evict(Wallets.class, w.getWalletId());
        em.refresh(w);
        return w;
    }
    private Wallets _getWallet(String address){
        Wallets w = (Wallets) em.createNamedQuery("Wallets.findByAddress")
                                .setParameter("address", address)
                                .getSingleResult();
        factory.getCache().evict(Wallets.class, w.getWalletId());
        em.refresh(w);
        return w;
    }

    private void _createHistoryEntry(BigDecimal amount, Wallets model, String type){
        System.out.println("Adding history entry");
        WalletsHistory entry = new WalletsHistory();
        entry.setAmount(amount);
        entry.setBalanceAfter(model.getBalance());
        entry.setComment(type);
        entry.setWallet(model);
        entry.setCreated(new Date());
        em.persist(entry);
        System.out.println("ready");

    }

    public void save(){
        em.merge(walletModel);
        System.out.println("merged");
    }
    
}
