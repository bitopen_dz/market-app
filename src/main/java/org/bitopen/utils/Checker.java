/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.bitopen.models.DepositsWatchlist;
import org.bitopen.exceptions.UnknownServiceException;
import org.bitopen.models.Wallets;
import org.bitopen.types.Transaction;
import org.bitopen.types.TransactionOutput;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Damian
 */
@Component
public class Checker {
    private static final int INTERVAL = 5000;
    private static EntityManagerFactory factory = null;
    HashMap <String, DefaultService> services = new HashMap<String, DefaultService>();
    EntityManager em;
    
    @PostConstruct
    void init(){
      factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
      services.put("BTC", new BitgoService());
    }
    
    @Scheduled(fixedDelay = INTERVAL)
    private void _check() {
        System.out.println("CHECKING NANANAA");
        em = factory.createEntityManager();
        em.getTransaction().begin();
        List<DepositsWatchlist> list = em
                .createNamedQuery("DepositsWatchlist.findByStatus")
                .setParameter("status", "PENDING")
                .getResultList();
        
        list.forEach((item) -> {
            factory.getCache().evict(DepositsWatchlist.class, item.getTxid());
            _makeCheck(item);
        });
        
        em.getTransaction().commit();
        em.close();
        
    }
    
    private void _makeCheck(DepositsWatchlist item){
       
        
       // item.setStatus("PROCESSING");
        item.setLastCheck(new Date());
        
        
        
        System.out.println("Sprawadzam "+item.getTxid());
        try{
            DefaultService service = services.get(item.getCurrencyId());
            if(service == null){
                throw new UnknownServiceException();
            }
            
            Transaction tx = service.getTransaction(item.getTxid());
            item.setConfirmations(tx.getConfirmations());
            if(item.getUserId() == null || item.getUserId() == 0){
                for(TransactionOutput output : tx.getOutputs()){
                    Wallets w = _addressExists(output.getAddress());
                    if( w != null){
                        item.setUserId(w.getUserId());
                        item.setAmount(output.getAmount());
                    }
                }
            }
            if(tx.getConfirmations() >= service.getMinimumConfirmations()) {
                _markAsConfirmed(item);
                _addFunds(tx, item);
            }
            _saveItem(item);
        }
        /**
         * @TODO: 
         * Dodać rózne typy wyjątków, żeby w niektórych przypadkach
         * oznaczać wpłatę jako błędną
         */
        catch(UnknownServiceException e){
            // noop
        }
        catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }
        
    }
    
    private Long _getUserByAddress(String address) {
        try{
            Wallet w = Wallet.byAddress(address, em);
            return w.getWalletModel().getUserId();
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    private void _markAsInvalid(DepositsWatchlist item){
        item.setStatus("INVALID");
    }
    
    private void _markAsConfirmed(DepositsWatchlist item){
        item.setStatus("SUCCESS");
    }
    
    private void _addFunds(Transaction tx, DepositsWatchlist item){
        System.out.println("adding funds");
        TransactionOutput[] outputs = tx.getOutputs();
        for(TransactionOutput output : outputs){
            System.out.println("checking address "+output.getAddress());
            Wallets w = _addressExists(output.getAddress());
            if( w != null){
                System.out.println("address "+output.getAddress()+" exists");
                _addFunds(output.getAddress(), output.getAmount() );

            }
        }
    }
    
    private Wallets _addressExists(String address) {
        try{
            List<Wallets> w = em
                    .createNamedQuery("Wallets.findByAddress")
                    .setParameter("address", address)
                    .getResultList();
            //em.close();
            if(w.size() > 0)
                return w.get(0);
            else
                return null;
        }
        catch(Exception e) {
            System.out.println(e);
            e.printStackTrace();
            return null;
        }
    }
    
    private void _addFunds(String address, BigDecimal amount) {
        Wallet w = Wallet.byAddress(address, em);
        System.out.println("?WTF?");
        w.addFunds(amount, "deposit");
        w.save();
    }

    
    private void _saveItem(DepositsWatchlist item){
        em.merge(item);
    }
}
