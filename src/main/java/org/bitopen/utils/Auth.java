/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.bitopen.exceptions.InvalidEmailException;
import org.bitopen.exceptions.TwoFactorTokenExpected;
import org.bitopen.models.Sessions;
import org.bitopen.models.Users;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dzach
 */
@RestController
public class Auth {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    private static SecureRandom random = new SecureRandom();
    private static String sessionKey;
    private static Users currentUser = null;
    private static EntityManagerFactory factory = null;

    public static Boolean authorize(String email, String password) throws TwoFactorTokenExpected {
        return Auth.authorize(email, password, "");
    }

    public static Boolean authorize(String email, String password, String token) throws TwoFactorTokenExpected {
        _initEntityManager();
        
        EntityManager em = factory.createEntityManager();
        try{
            Users user =  (Users) em.createNamedQuery("Users.findByEmail")
                            .setParameter("email", email)
                            .getSingleResult();
            factory.getCache().evict(Users.class, user.getUserId());
            //Users user = (Users) _user;
            if(user == null || !user.getStatus().equals("active")) {
                System.out.println("1");
                return false;
            }

            String hashedPassword = sha256(password);
            if(!user.isValidPassword(hashedPassword)){
                System.out.println("2");
                return false;
            }
            TwoFactor twoFactor = getTwoFactor(user);
            if(twoFactor.isEnabledForAction("login")) {
                if(token == null || token.length() == 0) {
                    throw new TwoFactorTokenExpected();
                }
                else if(!twoFactor.isValidToken(token)) {
                    return false;
                }
            }

            sessionKey = _addSession(user.getUserId());
            currentUser = user;

            em.close();
        
            return true;
        }
        catch(NoResultException e){
                System.out.println("3");
                System.out.println(e);
            return false;
        }
    }

    public static TwoFactor getTwoFactor(Users user) {
        return TwoFactor.forUser(user);
    }

    public static TwoFactor getTwoFactor() {
        return getTwoFactor(currentUser);
    }

    public static Boolean create(String email, String password) throws InvalidEmailException, Exception {
        _initEntityManager();
        if(!_isValidEmail(email)){
            throw new InvalidEmailException();
        }
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Users user = new Users();
        user.setEmail(email);
        user.setPassword( sha256(password) );
        user.setStatus("created");
        user.setActivationKey(_generateKey());

        try{
            em.persist(user);
            _sendActivationEmail(user);
            em.getTransaction().commit();
            em.close();
            return false;
        }
        catch(Exception e){
            Set<ConstraintViolation<?>> cv = ((ConstraintViolationException)e).getConstraintViolations();
            String[] messages = new String[cv.size()];
            Integer i = 0;
            for(ConstraintViolation<?> item : cv){
                messages[i] = item.getMessage();
                i++;
                System.out.println(item.getMessage());
            }
            //e.printStackTrace();
            em.getTransaction().rollback();
            em.close();
            throw new Exception(String.join(",", messages));
        }
    }
    
    
    public static Boolean retreiveSession(String token){
        _initEntityManager();
        
        EntityManager em = factory.createEntityManager();
        try{
            Sessions session = (Sessions)em.createNamedQuery("Sessions.findBySessionToken")
                                .setParameter("sessionToken", token)
                                .getSingleResult();
            if(session == null){
                return false;
            }

            Users user = (Users)em.createNamedQuery("Users.findByUserId")
                                .setParameter("userId", session.getUserId())
                                .getSingleResult();
            if(user == null){
                return false;
            }

            currentUser = user;

            em.close();
        }
        catch(NoResultException e){
            return false;
        }
        return true;
    }
    
    
    public static void logout(String token){
        _initEntityManager();
        
        EntityManager em = factory.createEntityManager();
        try{
            Sessions session = (Sessions)em.createNamedQuery("Sessions.findBySessionToken")
                                .setParameter("sessionToken", token)
                                .getSingleResult();
            
            em.getTransaction().begin();
            em.remove(session);
            em.getTransaction().commit();

            em.close();
        }
        catch(NoResultException e){
           
        }
    }
    public static void activate(String activationKey) {
        _initEntityManager();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        List usersList = em.createNamedQuery("Users.findByActivationKey")
                        .setParameter("activationKey", activationKey)
                        .getResultList();
        Users user = (Users) usersList.get(0);
        if(user != null){
            user.setStatus("active");
            em.merge(user);
        }
        em.getTransaction().commit();
        em.close();
    }
    public static String hashPassword(String pass) {
        return sha256(pass);
    }
    public static Boolean isAuthorized() {
        return currentUser != null;
    }
    
    public static Users getUser() {
        return currentUser;
    }
    
    public static void setUser(Users u) {
        currentUser = u;
    }

    public static String getSessionKey() {
        return sessionKey;
    }
    
    private static String _addSession(Long userId) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        
        String key = _generateKey();
        Sessions session = new Sessions(key, userId);
        //try {
            em.persist(session);
       // }
        //catch(Exception e){
           // System.out.println(e);
        //}
        try{
            em.getTransaction().commit();
        }
        catch(ConstraintViolationException e) {
            System.out.println(e);
        }
        em.close();
        
        return key;
    }
    
    private static String _generateKey(){
      return new BigInteger(390, random).toString(32);
    }
    
    
    private static void _initEntityManager(){
        factory = Persistence.createEntityManagerFactory("org.bitopen_BitOpen_jar_1.0-SNAPSHOTPU");
    }
    
    private static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
           throw new RuntimeException(ex);
        }
    }

    private static Boolean _isValidEmail(String email){
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.matches();
    }

    private static void _sendActivationEmail(Users user) {
       Email email = new Email();
       email.setTo(user.getEmail());
       email.setTitle("Aktywuj swoje konto w BitOpen");
       email.setContent("Witaj "+user.getEmail()+"! <br />Kliknij w poniższy link, aby aktywować Twoje konto "+
                        "na giełdzie BitOpen.pl! <br /><br />"+
                        "http://bitopen.pl/#index/user/activate/key-"+user.getActivationKey());
       email.send();
    }

}
