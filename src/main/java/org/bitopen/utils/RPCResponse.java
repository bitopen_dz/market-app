/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitopen.utils;

/**
 *
 * @author Damian
 */
public class RPCResponse {
    private Object result;
    private Object errors;
    private Integer id;

    public Object getResult() {
        return result;
    }

    public Object getErrors() {
        return errors;
    }

    public Integer getId() {
        return id;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RPCResponse{" + "result=" + result + ", errors=" + errors + ", id=" + id + '}';
    }
    
    
    
    
}

