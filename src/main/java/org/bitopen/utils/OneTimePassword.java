package org.bitopen.utils;

/**
 * Created by dzach on 14.01.2017.
 */

import org.apache.commons.codec.binary.Base32;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.lang.reflect.UndeclaredThrowableException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

/**
 * Created by zachmian on 28.12.16.
 */
public class OneTimePassword {
    private static final int[] DIGITS_POWER
            = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};
    private static final int PASSWORD_EXISTENCE_TIME = 30;

    private String hashingAlgorithm = "HmacSHA1";
    private Integer passwordLength = 6;
    private OneTimePasswordSecret secretKey;

    public OneTimePassword() {
    }

    public OneTimePassword(OneTimePasswordSecret secretKey) {
        this.secretKey = secretKey;
    }

    private static long getCurrentInterval() {
        long currentTimeSeconds = System.currentTimeMillis() / 1000;
        return currentTimeSeconds / PASSWORD_EXISTENCE_TIME;
    }

    public Integer generateCurrentPassword() {
        return generatePassword(getCurrentInterval());
    }

    public Integer generatePasswordForRelativePeriod(Integer step) {
        return generatePassword(getCurrentInterval() + step);
    }

    public Integer generatePassword(long time) {
        Base32 codec = new Base32();
        byte[] key = codec.decode(secretKey.getSecretKey());
        byte[] msg = ByteBuffer.allocate(8).putLong(time).array();
        byte[] hash = hmacSha(key, msg);

        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;

        int binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16) | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

        int otp = binary % DIGITS_POWER[passwordLength];

        return otp;
    }

    private byte[] hmacSha(byte[] keyBytes, byte[] text) {
        try {
            Mac hmac;
            hmac = Mac.getInstance(hashingAlgorithm);
            SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (GeneralSecurityException gse) {
            throw new UndeclaredThrowableException(gse);
        }
    }
}